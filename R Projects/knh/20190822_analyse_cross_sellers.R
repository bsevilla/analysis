# analysis cross sellers

#  =====  conversion days  =====
ggplot(cs) +
  geom_histogram(aes(x = conversion_days), bins = 60) +
  geom_vline(aes(xintercept = 15))

mean(cs$conversion_days)
median(cs$conversion_days)

#  =====  content  =====

content <- cs %>%
  group_by(genre_name, entry_product) %>%
  summarize(count = n())

#  =====  kino genre  =====

knh_genre_c <- cs %>% group_by(genre_name) %>% summarize(count = n())
knh_genre_r <- rcs %>% group_by(genre_name) %>% summarize(count = n())

ggplot() +
  geom_col(data = knh_genre_c, aes(x = genre_name, y = count), alpha = 0.5, fill = 'blue') +
  geom_col(data = knh_genre_r, aes(x = genre_name, y = count), alpha = 0.5, fill = 'red')

genres <- merge(knh_genre_c, knh_genre_r, by = 'genre_name', suffixes = c("_c", "_r")) %>%
  mutate(diff = 100*(count_c- count_r)/count_c)

genres_1 <- genres
genres_2 <- genres

#  =====  qué pelis/musicales  =====

movie <- cs %>% filter(genre_name == 'Romántica') %>% 
  group_by(film_name, first_event_name) %>% summarize(count = n())

movies_c <- cs %>% group_by(film_name) %>% summarize(count = n())
movies_r <- rcs %>% group_by(film_name) %>% summarize(count = n())

movies <- merge(movies_c, movies_r, by = 'film_name', suffixes = c("_c", "_r")) %>%
  mutate(diff = (count_c- count_r),
         pct = 100*(count_c- count_r)/count_c)


movies_c <- cs %>% group_by(film_name_last) %>% summarize(count = n())
movies_r <- rcs %>% group_by(film_name_last) %>% summarize(count = n())

movies <- merge(movies_c, movies_r, by = 'film_name_last', suffixes = c("_c", "_r")) %>%
  mutate(diff = (count_c- count_r),
         pct = 100*(count_c- count_r)/count_c) %>%
  filter(pct > 40)

top <- cs %>% filter(film_name_last %in% pelis) %>% 
  group_by(film_name_last, first_event_name) %>% 
  summarize(count = n())
movies_m <- movies %>% mutate(diffpct = diff*pct/100,
                              countcpct = count_c*pct/100)


#  =====  productos de entrada para pelis muy crossellosas  =====

top_cs <- spc %>% group_by(first_event_name, movie) %>% summarize(count = n())
