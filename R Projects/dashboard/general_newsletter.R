# general newsletter 

query <- sprintf("select ist.i_status, fst.f_status, count(ci.customer_number) as 'count'
from user_bsevilla.v_users ci
left join (
		select a.customer_number as customer_number, min(a.i_status) as i_status
		from (
				select ci.customer_number as customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when (ci.first_order_date < %sand nl.subscription_date < %s and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > %s and ci.last_interaction > %s) then
					  	1 --'i_buyer' -- buyer subscriber pero no pasa nada 
					when ci.first_order_date < %s and ci.last_interaction > %s then
						1 --'i_buyer'
					when nl.subscription_date < %s and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > %s and ci.last_interaction > %s then 
						2 --'i_subscriber'
					when (ci.first_order_date > %s or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > %s then
						3 --'i_visitor'
					else 100 end as i_status
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL'
				and nl.newsletter_type = 'General NL'
				) a
		group by 1) a on a.customer_number = ci.customer_number
left join (
		select a.customer_number, min(a.f_status) as f_status
		from (
				select ci.customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when ci.last_interaction > %s and ci.last_interaction < %s then
						0--'f_dead'
					when (ci.first_order_date > %s and ci.first_order_date < %s) then
						1--'f_new_buyer'
					when (nl.unsubscription_date > %s and nl.unsubscription_date < %s) then
						2--'f_unsubscribed'
					when (ci.first_order_date < %s and ci.last_interaction > %s) then
						3--'f_still_buyer'
					when (ci.first_order_date > %s or ci.first_order_date is null) and nl.subscription_date < %s and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > %s and ci.last_interaction > %s then
						4--'f_still_subscriber'
					when (ci.first_order_date > %s or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > %s then
						5--'f_still_visitor'
					when (ci.first_order_date > %s or ci.first_order_date is null) and nl.subscription_date > %s and nl.subscription_date < %s and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > %s and ci.last_interaction > %s then
						6--'f_new_subscriber'
					else 100 end as f_status					
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL'
				and nl.newsletter_type = 'General NL'
				) a
		group by 1) b on b.customer_number = ci.customer_number
left join user_bsevilla.v_dim_i_status ist on a.i_status = ist.i_status_id
left join user_bsevilla.v_dim_f_status fst on b.f_status = fst.f_status_id
where ci.leading_system <> 'ENL'
and not (a.i_status = 100 and b.f_status = 100)
group by 1,2", date_init, date_init, date_init, date_active_init, date_init, date_active_init, date_init, date_init, date_active_init, date_init, date_active_init, date_active_init, date_active_fin, date_init, date_fin, date_init, date_fin, date_init, date_active_fin, date_fin, date_init, date_fin, date_active_fin, date_fin, date_active_fin, date_fin, date_init, date_fin, date_fin, date_active_fin)

con <- dbConnect("exa", dsn="exadb")
df <- dbGetQuery(con, query)
dbDisconnect(con)
rm(con)

colnames(df) <- tolower(colnames(df))

general_newsletter <- df %>% mutate(
  genre = rep('gen_nl', nrow(df)),
  month = rep(date_month, nrow(df))
) %>% select(month, genre, i_status, f_status, count)

rm(df)
