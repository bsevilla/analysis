library(tidyverse)
library(lubridate)


query <- "select avg(days_between(ohs.order_2, ohs.order_1)) as 'day_order_2'
from user_bsevilla.v_users ci
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as order_1, 
		        max(case when rnAsc = 2 then order_date end) as order_2
		from    (
		        select  customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        )
		group by
		        customer_number
		) ohs on ohs.customer_number = ci.customer_number
where ci.leading_system in ('WEB','EVI')
and ohs.order_2 > '2019-07-01'
and ohs.order_2 < '2019-08-01'"

dates <- substring(query, str_locate_all(pattern ="'201", query)[[1]][,1], str_locate_all(pattern ="'201", query)[[1]][,1]+11)

unique(dates)

q_replaced <- str_replace_all(query, setNames(rep('%s', length(dates)), dates))

date_init <- as.Date('2019-07-01', origin="1970-01-01")
date_fin <- date_init + months(1)
date_active_init <- date_init - years(3)
date_active_fin <- date_fin - years(3)

date_month = as.character(date_init, '%Y%m')

date_init <- paste("'", as.character(date_init), "'", sep = '')
date_fin <- paste("'", as.character(date_fin), "'", sep = '')
date_active_init <- paste("'", as.character(date_active_init), "'", sep = '')
date_active_fin <- paste("'", as.character(date_active_fin), "'", sep = '')

d <- tibble(dates)
colnames(d) <- 'date'
datenames <- mutate(d,
  date_names = ifelse(d == date_init, 'date_init',
               ifelse(d == date_fin, 'date_fin',
               ifelse(d == date_active_init, 'date_active_init',
               ifelse(d == date_active_fin, 'date_active_fin', 'error'))))
)$date_names

datenames = paste(datenames, collapse = ', ')
cat(paste("sprintf(\"",q_replaced, "\", ", datenames, ")", sep = ""))

rm(d, datenames, q_replaced,dates)

