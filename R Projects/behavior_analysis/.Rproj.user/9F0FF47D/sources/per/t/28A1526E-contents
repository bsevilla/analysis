library(tidyverse)
library(exasol)
library(RODBC)
library(stats)
library(lubridate)
library(beepr)
library(DescTools)
options(scipen = 10000)
`%ni%` <- negate(`%in%`)

# en general

query <- "select to_char(send_date,'YYYY-MM-DD') as 'date', 
		case when csr.target_group_name like '%RRE1%' then 'reco1'
			 when csr.target_group_name like '%RRE2%' then 'reco2' end as reco_group,
		sum(csr.opt_net) as opt_net, sum(csr.u_opens) as u_opens, sum(csr.u_opens)/sum(csr.opt_net) as open_rate,
		sum(combined_tix) as comb_tx,
		sum(combined_tix)/sum(csr.opt_net) as cvr
from zone_country_es.v_c_dama_csr csr
--left join zone_country_es.v_c_optf__comb__mailingrecipients mr on mr.mailingid = csr.mailing_id
--left join zone_country_es.v_c_optf__comb__opens mo on mo.mailingid = mr.mailingid and mo.pii_userid = mr.pii_userid
where lower(csr.mailing_type_name) like '%insider%'
and csr.send_date > '2017-09-23'
and (csr.target_group_name like '%RRE1%' or csr.target_group_name like '%RRE2%')
and csr.opt_net > 10
and lower(csr.mailing_name) not like '%test%'
and lower(csr.mailing_name) not like '%edit%'
and csr.mailing_name not like '%_teatro%'
group by 1,2
order by 1,2
"

con <- dbConnect("exa", dsn="exadb")
df <- dbGetQuery(con, query)
beep()
dbDisconnect(con)
rm(con)

colnames(df) <- tolower(colnames(df))

df$date <- ymd(df$date)

df_monthly <- df %>% 
  mutate(date_month = ymd(paste(as.character(year(date)), as.character(month(date)), '01', sep = '-'))) %>%
  group_by(date_month, reco_group) %>% summarize(open_rate = sum(u_opens)/sum(opt_net),
                                                 cvr = sum(comb_tx)/sum(opt_net))

df_s <- merge(filter(df_monthly, reco_group == 'reco1'), filter(df_monthly, reco_group == 'reco2'),
              by = 'date_month', suffixes = c('_r1', '_r2')) %>% 
  select(date_month, open_rate_r1, open_rate_r2, cvr_r1, cvr_r2) %>%
  mutate(ok_or = ifelse(open_rate_r1 < open_rate_r2, 1, 0),
         ok_cvr = ifelse(cvr_r1 < cvr_r2, 1, 0))

df_ms <- merge(df_monthly, df_s, by = 'date_month') %>% select(date_month, reco_group, open_rate, cvr, ok_or, ok_cvr)

ggplot(df_ms) +
  geom_line(aes(x = date_month, y = 100*open_rate, color = reco_group, group = reco_group), size = 1.2) +
  geom_point(aes(x = date_month, y = ok_or+10), color = 'darkgrey', alpha = 0.4, size = 7, shape = 15) +
  scale_color_manual(values = c('darkred', 'red')) +
  ggtitle('open rate')

ggplot(df_ms) +
  geom_line(aes(x = date_month, y = 1000*cvr, color = reco_group, group = reco_group), size = 1.2) +
  geom_point(aes(x = date_month, y = ok_cvr/2-0.5), color = 'darkgrey', alpha = 0.4, size = 7, shape = 15) +
  scale_color_manual(values = c('darkred', 'red')) +
  ggtitle('cvr')


# las personitas
  
query <- "select case when csr.target_group_name like '%RRE1%' then 'reco1'
		    when csr.target_group_name like '%RRE2%' then 'reco2' end as reco_group,
		ci.reco_level, ci.order_cnt_sht, ci.order_cnt_mid, ci.order_cnt_lng, ci.order_cnt_all, u.suffix, u.gender, u.age_range, 
ci.monetary_value_mid, ci.monetary_value_lng, ci.monetary_value_sht, ci.monetary_value_all, ci.ticket_price_avg_all, 
u.entry_product, ci.status_newsletter, ci.newsletter_open_time_avg, u.registration_date, u.last_interaction,
case when o.max_ords < 4 then 1 else 0 end as is_ok
from zone_country_es.v_c_dama_csr csr
left join zone_country_es.v_c_optf__comb__mailingrecipients mr on mr.mailingid = csr.mailing_id
--left join zone_country_es.v_c_optf__comb__opens mo on mo.mailingid = mr.mailingid and mo.pii_userid = mr.pii_userid
left join zone_country_es.v_c_dama_ci ci on mr.pii_userid = ci.customer_email
left join user_bsevilla.v_users u on ci.customer_number = u.customer_number
left join (
			select customer_number, max(ords) as max_ords
			from (
					select customer_number, evid, count(order_number) as ords
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o on o.customer_number = u.customer_number
where lower(csr.mailing_type_name) like '%insider%'
and csr.send_date > '2018-12-01'
and csr.send_date < '2018-12-12'
and (csr.target_group_name like '%RRE1%' or csr.target_group_name like '%RRE2%')
and csr.opt_net > 10
and lower(csr.mailing_name) not like '%test%'
and lower(csr.mailing_name) not like '%edit%'
and csr.mailing_name not like '%_teatro%'
and ci.leading_system in ('WEB', 'EVI')"

con <- dbConnect("exa", dsn="exadb")
df <- dbGetQuery(con, query)
beep()
dbDisconnect(con)
rm(con)

colnames(df) <- tolower(colnames(df))


df$registration_date <- as.numeric(ymd_hms(df$registration_date))
df$last_interaction <- as.numeric(ymd_hms(df$last_interaction))

df_mc <- df %>% select(order_cnt_sht, order_cnt_mid, order_cnt_lng, order_cnt_all, monetary_value_mid, monetary_value_lng, monetary_value_sht, monetary_value_all, ticket_price_avg_all, newsletter_open_time_avg, registration_date, last_interaction, reco_group, is_ok, reco_level, gender, age_range, entry_product)
df_mc$reco_group = ifelse(df_mc$reco_group == 'reco1', 0,1)
df_mc$gender = ifelse(df_mc$gender == 'male', 1,
               ifelse(df_mc$gender == 'female',2, 0))
df_mc$age_range = ifelse(df_mc$age_range == '18-24',1,
                  ifelse(df_mc$age_range == '25-34',2,
                  ifelse(df_mc$age_range == '35-44',3,
                  ifelse(df_mc$age_range == '55-90',4, 0))))
df_mc$entry_product = ifelse(df_mc$entry_product == 'Musical', 1,
                      ifelse(df_mc$entry_product == 'Culture', 2,
                      ifelse(df_mc$entry_product == 'Música',  3,
                      ifelse(df_mc$entry_product == 'Deportes',4,
                      ifelse(df_mc$entry_product == 'Cinema',  5,0)))))


tic('cor')                  #c = 1:12, p = 15:18, d = 13:14
mc <- mixedCor(data = df_mc, c = 1:12, p = 17, d = 13:14)
toc()
beep()

corrplot(mc$rho, method = 'circle')

df_mc <- df %>% select(order_cnt_sht, order_cnt_all, monetary_value_sht, monetary_value_all, ticket_price_avg_all, newsletter_open_time_avg, registration_date, last_interaction, reco_group, is_ok, gender, age_range, entry_product)

library(klaR)
library(caret)
library(e1071)

df_mc$reco_group <- as.factor(df_mc$reco_group)
df_mc$is_ok <- as.factor(df_mc$is_ok)
df_mc$gender <- as.factor(df_mc$gender)
df_mc$age_range <- as.factor(df_mc$age_range)
df_mc$entry_product <- as.factor(df_mc$entry_product)

df_mc[is.na(df_mc)] <- 0

boxplot(scale(df_mc[,1:8]), col = 'orange', main = 'Features Boxplot')


index = sample(nrow(df_mc), floor(nrow(df_mc) * 0.7)) #70/30 split.
train = df_mc[index,]
test = df_mc[-index,]

xTrain = train %>% dplyr::select(order_cnt_sht, order_cnt_all, monetary_value_sht, monetary_value_all, ticket_price_avg_all, newsletter_open_time_avg, registration_date, last_interaction, is_ok, gender, age_range, entry_product) # removing y-outcome variable.
yTrain = train$reco_group # only y.

xTest = test %>% dplyr::select(order_cnt_sht, order_cnt_all, monetary_value_sht, monetary_value_all, ticket_price_avg_all, newsletter_open_time_avg, registration_date, last_interaction, is_ok, gender, age_range, entry_product)
yTest = test$reco_group

tic('modelo')
model = train(xTrain,yTrain,'tan',trControl=trainControl(method='cv',number=10))
toc()
beep()

prop.table(table(predict(model$finalModel,xTest)$class,yTest))

x <- df_mc 
y <- df_mc$reco_group


# la vieja usanza
df_mc <- df %>% select(order_cnt_sht, order_cnt_all, monetary_value_sht, monetary_value_all, ticket_price_avg_all, newsletter_open_time_avg, registration_date, last_interaction, reco_group, is_ok, gender, age_range, entry_product)
df_safe <- df
df <- df_mc
rm(df_mc)

ggplot(df) +
  geom_density(aes(x = order_cnt_sht)) +
  facet_grid(reco_group ~ .)

ggplot(data = df %>% mutate(bin = cut(order_cnt_sht, breaks = c(-1,2,10,50,Inf), labels = c('0-1', '2-10', '11-50', '51+'))), aes(x = ocs_bin, group = reco_group)) +
  geom_bar(aes(y = ..prop.., fill = factor(..x..)), stat="count") +
  geom_text(aes( label = scales::percent(..prop..),
                 y= ..prop.. ), stat= "count") +
  facet_grid(reco_group ~ .)

wilcox_test(order_cnt_all ~ factor(reco_group), data = df)
