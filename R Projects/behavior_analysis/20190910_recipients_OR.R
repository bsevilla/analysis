# emails opt_net or

query <- "select mailing_name, opt_net, combined_tix/opt_net, mailing_type_name
from zone_country_es.v_c_dama_csr
where send_date > '2018-09-10'
and lower(mailing_name) not like '%resend%'
and (lower(mailing_type_name) like '%insider%'
or lower(mailing_type_name) like '%regio%'
or lower(mailing_type_name) like '%special%')
and opt_net > 50
"

con <- dbConnect("exa", dsn="exadb")
df <- dbGetQuery(con, query)
beep()
dbDisconnect(con)
rm(con)

colnames(df) <- c('name','opt_net', 'or', 'type')

ggplot(df) +
  geom_point(aes(x = log(opt_net), y = log(or), color = type)) +
  geom_smooth(aes(x = log(opt_net), y = log(or))) +
  facet_grid(rows=vars(type))
  # geom_smooth(aes(x = log(opt_net), y = or, group = type))


