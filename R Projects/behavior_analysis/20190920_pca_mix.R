library(PCAmixdata)
library(tidyverse)
library(exasol)
library(RODBC)
library(stats)
library(lubridate)
library(beepr)
library(DescTools)
options(scipen = 10000)
`%ni%` <- negate(`%in%`)

query <- "select ci.reco_level, ci.order_cnt_sht, ci.order_cnt_mid, ci.order_cnt_lng, ci.order_cnt_all, u.suffix, u.gender, u.age_range, 
ci.monetary_value_mid, ci.monetary_value_lng, ci.monetary_value_sht, ci.monetary_value_all, ci.ticket_price_avg_all, 
u.entry_product, ci.status_newsletter, ci.newsletter_open_time_avg, u.registration_date, u.last_interaction
from user_bsevilla.v_users u
left join zone_country_es.v_c_dama_ci ci on u.customer_number = ci.customer_number
left join (
			select customer_number, max(ords) as max_ords
			from (
					select customer_number, evid, count(order_number) as ords
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o on o.customer_number = u.customer_number
where u.leading_system in ('WEB','EVI')
and last_interaction > '2016-09-20'
and o.max_ords < 4
and u.suffix <> 'entradas.com'
order by rand()
limit 10000"

con <- dbConnect("exa", dsn="exadb")
df <- dbGetQuery(con, query)
beep()
dbDisconnect(con)
rm(con)

colnames(df) <- tolower(colnames(df))

df$registration_date <- as.numeric(ymd_hms(df$registration_date))
df$last_interaction <- as.numeric(ymd_hms(df$last_interaction))


df_quanti <- df %>% select(reco_level, order_cnt_sht, order_cnt_mid, order_cnt_lng, order_cnt_all,
                          monetary_value_sht, monetary_value_mid, monetary_value_lng, monetary_value_all,
                          ticket_price_avg_all, newsletter_open_time_avg, registration_date, last_interaction)
df_quali <- df %>% select(suffix, gender, age_range, entry_product)

df.pca <- PCAmix(X.quanti = df_quanti, X.quali = df_quali, rename.level = T)

# kmeans
df_km <- df %>% mutate(
  suffix = as.factor(suffix),
  gender = as.factor(gender),
  age_range = as.factor(age_range),
  entry_product = as.factor(entry_product)
)
gower_dist <- daisy(df_km, metric = "gower")

tic('gower_mat')
gower_mat <- as.matrix(gower_dist)
toc()
beep()

# most similar
df_km[which(gower_mat == min(gower_mat[gower_mat != min(gower_mat)]), arr.ind = TRUE)[1, ], ]
# most dissimilar
df_km[which(gower_mat == max(gower_mat[gower_mat != max(gower_mat)]), arr.ind = TRUE)[1, ], ]

# clust
sil_width <- c(NA)
for(i in 2:8){  
  pam_fit <- pam(gower_dist, diss = TRUE, k = i)  
  sil_width[i] <- pam_fit$silinfo$avg.width  
}

plot(1:8, sil_width,
     xlab = "Number of clusters",
     ylab = "Silhouette Width")
lines(1:8, sil_width)

k <- 3

pam_fit <- pam(gower_dist, diss = TRUE, k)
pam_results <- df_km %>%
  mutate(cluster = pam_fit$clustering) %>%
  group_by(cluster) %>%
  do(the_summary = summary(.))


pam_results$the_summary

tsne_obj <- Rtsne(gower_dist, is_distance = TRUE)
tsne_data <- tsne_obj$Y %>%
  data.frame() %>%
  setNames(c("X", "Y")) %>%
  mutate(cluster = factor(pam_fit$clustering))
ggplot(aes(x = X, y = Y), data = tsne_data) +
  geom_point(aes(color = cluster))


