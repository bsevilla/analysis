-- orders

select case when coalesce(oh.order_cnt, 0) = 0 then 0
	when coalesce(oh.order_cnt, 0) = 1 then 1
	when coalesce(oh.order_cnt, 0) = 2 then 2
	when coalesce(oh.order_cnt, 0) > 2 then 3 end as order_cnt,
	count(ci.customer_number) as 'count',
	avg(ohs.order_2) as 'days_order_2',
	avg(ohs.order_3) as 'days_order_3'
from user_bsevilla.v_users ci
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-06-01'
		group by 1
		) oh on oh.customer_number = ci.customer_number
left join 
		(select  customer_number,
		        days_between(max(case when rnAsc = 2 then order_date end), max(case when rnAsc = 1 then order_date end)) as order_2,
		        days_between(max(case when rnAsc = 3 then order_date end), max(case when rnAsc = 2 then order_date end)) as order_3
		from    (
		        select  customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where order_date < '2019-06-01'
		        )
		group by
		        customer_number
		) ohs on ohs.customer_number = ci.customer_number
where ci.leading_system in ('WEB', 'EVI')
group by 1
;


        