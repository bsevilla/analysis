-- activity

select ist.i_status, 
	   case when b.f_status = 0 then 'f_inactive' else 'f_active' end as f_status, 
	   avg(days_between(ci.last_interaction, ci.registration_date)) as lifetime,
	   count(ci.customer_number) as 'count'
from user_bsevilla.v_users ci
left join (
		select a.customer_number as customer_number, min(a.i_status) as i_status
		from (
				select ci.customer_number as customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when (ci.first_order_date < '2019-08-01'and nl.subscription_date < '2019-08-01' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-08-01' and ci.last_interaction > '2016-08-01') then
					  	1 --'i_buyer' -- buyer subscriber pero no pasa nada 
					when ci.first_order_date < '2019-08-01' and ci.last_interaction > '2016-08-01' then
						1 --'i_buyer'
					when nl.subscription_date < '2019-08-01' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-08-01' and ci.last_interaction > '2016-08-01' then 
						2 --'i_subscriber'
					when (ci.first_order_date > '2019-08-01' or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > '2016-08-01' then
						3 --'i_visitor'
					else 100 end as i_status
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) a on a.customer_number = ci.customer_number
left join (
		select a.customer_number, max(a.f_status) as f_status
		from (
				select ci.customer_number,
					case when (coalesce(olast.last_order_date,'1900-01-01 00:00:00.0') > add_months('2019-08-01', -6)
							or coalesce(nlast.last_subscription_Date, '1900-01-01 00:00:00.0') > add_months('2019-08-01', -6)
							or coalesce(ci.last_open_date_newsletter, '1900-01-01 00:00:00.0') > add_months('2019-08-01', -3)) then
						1
					else 0 end as f_status
				from user_bsevilla.v_users ci
				left join ( 
							select customer_number, max(subscription_date) as last_subscription_date
							from user_bsevilla.v_newsletters
							where subscription_date < '2019-08-01'
							group by 1	
						  ) nlast on nlast.customer_number = ci.customer_number
				left join ( 
							select customer_number, max(order_date) as last_order_date
							from user_bsevilla.v_orders
							where order_date < '2019-08-01'
							group by 1	
						  ) olast on olast.customer_number = ci.customer_number	  
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) b on b.customer_number = ci.customer_number
left join user_bsevilla.v_dim_i_status ist on a.i_status = ist.i_status_id
where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
and not (a.i_status = 100 and b.f_status = 100)
group by 1,2
;





