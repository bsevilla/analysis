-- one time buyers

select 
	case when coalesce(ioh.order_cnt, 0) = 1 and ci.last_interaction > '2016-07-01' then 
		'i_otb'
	when coalesce(ioh.order_cnt, 0) > 1 and ci.last_interaction > '2016-07-01' then 
		'i_more'
	when coalesce(ioh.order_cnt, 0) = 0 and ci.last_interaction > '2016-07-01' then 
		'i_zero' 
	else 'other' end as i_status,
	case when ci.last_interaction > '2016-07-01' and ci.last_interaction < '2016-08-01' then 
		'f_dead'
	when coalesce(foh.order_cnt, 0) = 1 and ci.last_interaction > '2016-08-01' then
		case when ci.registration_date < '2019-07-01' then
			'f_otb'
		else 'f_new_otb' end
	when coalesce(foh.order_cnt, 0) > 1 and ci.last_interaction > '2016-08-01' then 
		'f_more'
	when coalesce(foh.order_cnt, 0) = 0 and ci.last_interaction > '2016-08-01' then 
		'f_zero' 
	else 'other' end as f_status,
	count(ci.customer_number)
from user_bsevilla.v_users ci
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-07-01'
		group by 1
		) ioh on ioh.customer_number = ci.customer_number
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-08-01'
		group by 1
		) foh on foh.customer_number = ci.customer_number
where ci.leading_system in ('WEB','EVI')
group by 1,2
order by 1,2
;

-- date order 2

select avg(days_between(ohs.order_2, ohs.order_1)) as 'day_order_2'
from user_bsevilla.v_users ci
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as order_1, 
		        max(case when rnAsc = 2 then order_date end) as order_2
		from    (
		        select  customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        )
		group by
		        customer_number
		) ohs on ohs.customer_number = ci.customer_number
where ci.leading_system in ('WEB','EVI')
and ohs.order_2 > '2019-07-01'
and ohs.order_2 < '2019-08-01'
;     