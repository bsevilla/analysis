-- mailing KPIs 2019

select 
	case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar'
	when lower(mailing_type_name) like '%birthday%' then
	  'birthday'
	when lower(mailing_type_name) like '%abandonment%' then
	  'cart abandonment'
	else 'rest'
	end as mailing_type,
	to_char(send_date, 'YYYYMM') as send_month,
	/*avg(opt_net) as opt_net,
	avg(u_opens)/avg(opt_net) as "OR",
	avg(u_clicks)/avg(opt_net) as ctr,
	avg(unsubscriptions)/avg(opt_net) as unsubs,
	sum(crm_tix) as crm_tix,*/
	sum(crm_tix_direct) as crm_tix_direct,
	sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-01'
and send_date < '2019-01-01'
/*and (lower(mailing_type_name) like '%insider%'
or lower(mailing_type_name) like '%regio%'
or lower(mailing_type_name) like '%special%'
or lower(mailing_type_name) like '%cinema%'
or lower(mailing_type_name) like '%similar%')*/
group by 1,2
order by 1,2
;

-- mailing KPIs 2019 INCLUYENDO RESENDS

select 
	case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar'
	end as mailing_type,
	to_char(send_date, 'YYYYMM') as send_month,
	avg(u_opens)/avg(opt_net) as "OR",
	avg(u_clicks)/avg(opt_net) as ctr,
	avg(unsubscriptions)/avg(opt_net) as unsubs,
	sum(crm_tix) as crm_tix,
	sum(crm_tix_direct) as crm_tix_direct,
	sum(combined_tix) as combined_tix
from (
		select csr.send_date, 
			   csr.mailing_name,
			   csr.mailing_type_name,
			   case when csr.opt_net >= csr_rs.opt_net then csr.opt_net else csr_rs.opt_net end as opt_net,
			   csr.u_opens + csr_rs.u_opens as u_opens,
			   csr.u_clicks + csr_rs.u_clicks as u_clicks,
			   csr.unsubscriptions + csr_rs.unsubscriptions as unsubscriptions,
			   csr.crm_tix + csr_rs.crm_tix as crm_tix,
			   csr.crm_tix_direct + csr_rs.crm_tix_direct as crm_tix_direct,
			   csr.combined_tix + csr_rs.combined_tix as combined_tix
		from zone_country_es.v_c_dama_csr csr
		left join (
					select left(mailing_name, length(mailing_name)-7) as mailing_name_rs, mailing_name, 
					opt_net, u_opens, u_clicks, unsubscriptions, crm_tix, crm_tix_direct, combined_tix
					from zone_country_es.v_c_dama_csr
					where (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%') 
					and (lower(mailing_name) like '%resend%' or lower(mailing_name) like '%noopen%' or lower(mailing_name) like '%reopen%')
					) csr_rs on csr.mailing_name = csr_rs.mailing_name_rs
		where 1=1
		and csr_rs.mailing_name is not null
		and send_date > '2019-01-01'
		--and send_date < '2019-01-01'		
		union
		select send_date,
				mailing_name,
				mailing_type_name,
				opt_net,
				u_opens,
				u_clicks,
				unsubscriptions,
				crm_tix,
				crm_tix_direct,
				combined_tix
		from zone_country_es.v_c_dama_csr
		where 1=1
		and mailing_name not in (select distinct mailing_name_rs
								  from zone_country_es.v_c_dama_csr csr
								  left join (
												select left(mailing_name, length(mailing_name)-7) as mailing_name_rs
												from zone_country_es.v_c_dama_csr
												where (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%') 
												and (lower(mailing_name) like '%resend%' or lower(mailing_name) like '%noopen%' or lower(mailing_name) like '%reopen%')
											) csr_rs on csr.mailing_name = csr_rs.mailing_name_rs
								  where csr_rs.mailing_name_rs is not null
								union
								  select distinct csr_rs.mailing_name
								  from zone_country_es.v_c_dama_csr csr
								  left join (
												select left(mailing_name, length(mailing_name)-7) as mailing_name_rs, mailing_name
												from zone_country_es.v_c_dama_csr
												where (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%') 
												and (lower(mailing_name) like '%resend%' or lower(mailing_name) like '%noopen%' or lower(mailing_name) like '%reopen%')
											) csr_rs on csr.mailing_name = csr_rs.mailing_name_rs
								  where csr_rs.mailing_name_rs is not null
								)
		--and (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%')
		and send_date > '2019-01-01'
		--and send_date < '2019-01-01'
		and (lower(mailing_type_name) like '%insider%'
					or lower(mailing_type_name) like '%regio%'
					or lower(mailing_type_name) like '%special%'
					or lower(mailing_type_name) like '%cinema%'
					or lower(mailing_type_name) like '%similar%')
	 )
group by 1,2
order by 1,2
;

-- union			
		select csr.send_date, 
			   csr.mailing_name,
			   csr.mailing_type_name,
			   case when csr.opt_net >= csr_rs.opt_net then csr.opt_net else csr_rs.opt_net end as opt_net,
			   csr.u_opens + csr_rs.u_opens as u_opens,
			   csr.u_clicks + csr_rs.u_clicks as u_clicks,
			   csr.unsubscriptions + csr_rs.unsubscriptions as unsubscriptions,
			   csr.crm_tix + csr_rs.crm_tix as crm_tix,
			   csr.crm_tix_direct + csr_rs.crm_tix_direct as crm_tix_direct,
			   csr.combined_tix + csr_rs.combined_tix as combined_tix
		from zone_country_es.v_c_dama_csr csr
		left join (
					select left(mailing_name, length(mailing_name)-7) as mailing_name_rs, mailing_name, 
					opt_net, u_opens, u_clicks, unsubscriptions, crm_tix, crm_tix_direct, combined_tix
					from zone_country_es.v_c_dama_csr
					where (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%') 
					and (lower(mailing_name) like '%resend%' or lower(mailing_name) like '%noopen%' or lower(mailing_name) like '%reopen%')
					) csr_rs on csr.mailing_name = csr_rs.mailing_name_rs
		where 1=1
		and csr_rs.mailing_name is not null
		and send_date > '2018-01-01'
		--and send_date < '2019-01-01'		
		union
		select send_date,
				mailing_name,
				mailing_type_name,
				opt_net,
				u_opens,
				u_clicks,
				unsubscriptions,
				crm_tix,
				crm_tix_direct,
				combined_tix
		from zone_country_es.v_c_dama_csr
		where 1=1
		and mailing_name not in (select distinct mailing_name_rs
								  from zone_country_es.v_c_dama_csr csr
								  left join (
												select left(mailing_name, length(mailing_name)-7) as mailing_name_rs
												from zone_country_es.v_c_dama_csr
												where (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%') 
												and (lower(mailing_name) like '%resend%' or lower(mailing_name) like '%noopen%' or lower(mailing_name) like '%reopen%')
											) csr_rs on csr.mailing_name = csr_rs.mailing_name_rs
								  where csr_rs.mailing_name_rs is not null
								union
								  select distinct csr_rs.mailing_name
								  from zone_country_es.v_c_dama_csr csr
								  left join (
												select left(mailing_name, length(mailing_name)-7) as mailing_name_rs, mailing_name
												from zone_country_es.v_c_dama_csr
												where (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%') 
												and (lower(mailing_name) like '%resend%' or lower(mailing_name) like '%noopen%' or lower(mailing_name) like '%reopen%')
											) csr_rs on csr.mailing_name = csr_rs.mailing_name_rs
								  where csr_rs.mailing_name_rs is not null
								)
		--and (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%')
		and send_date > '2018-01-01'
		--and send_date < '2019-01-01'
		and (lower(mailing_type_name) like '%insider%'
					or lower(mailing_type_name) like '%regio%'
					or lower(mailing_type_name) like '%special%'
					or lower(mailing_type_name) like '%cinema%'
					or lower(mailing_type_name) like '%similar%')
		
		
;
	
	
	
	
	
	
---------------- GROSS RECIPIENTS 
with t as 
(	
select 
	case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar'
	when lower(mailing_type_name) like '%birthday%' then
	  'birthday'
	when lower(mailing_type_name) like '%abandonment%' then
	  'cart abandonment'
	else 'rest'
	end as mailing_type,
	to_char(send_date, 'YYYYMM') as send_month,
	avg(opt_net) as avgoptnet,
	sum(opt_net) as grossoptnet,
	avg(u_opens)/avg(opt_net) as "OR",
	avg(u_clicks)/avg(opt_net) as ctr,
	avg(unsubscriptions)/avg(opt_net) as unsubs,
	sum(crm_tix) as crm_tix,
	sum(crm_tix_direct) as crm_tix_direct,
	sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2019-01-01'
--and send_date < '2019-01-01'
group by 1,2
order by 1,2
)
select t.mailing_type, t.send_month,
	   case when (t.mailing_type = 'insider' or t.mailing_type = 'regio') 
		   	then t.avgoptnet 
		   	else t.grossoptnet end as opt_net,
	   t.or, t.ctr, t.unsubs, t.crm_tix, t.crm_tix_direct, t.combined_tix
	   from t
;

select case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar'
	when lower(mailing_type_name) like '%birthday%' then
	  'birthday'
	when lower(mailing_type_name) like '%abandonment%' then
	  'cart abandonment'
	else 'rest'
	end as mailing_type, 
	to_char(send_date, 'YYYYMM') as send_month, 
	case when (lower(mailing_type_name) like '%insider%' or lower(mailing_type_name) like '%regio%')
	then avg(opt_net) else sum(opt_net) end as opt_net
from zone_country_es.v_c_dama_csr
where 1=1
and send_date > '2019-01-01'
group by 1,2
;



