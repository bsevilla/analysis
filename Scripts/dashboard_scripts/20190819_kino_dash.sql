--select count(customer_id), count(distinct customer_id) from (
select 
	case when (iohs.web_first_order_date > '2019-07-01' or iohs.web_first_order_date is null) and ci.last_interaction > '2016-07-01' then
		'i_knh'
	when iohs.web_first_order_date > iohk.knh_first_order_date and iohs.web_first_order_date < '2019-07-01' and ci.last_interaction > '2016-07-01' then
		'i_knh->web'
	else 'other' end as i_status,
	case when ci.last_interaction > '2016-07-01' and ci.last_interaction < '2016-08-01' then
		'f_dead'
	when fohk.knh_first_order_date > '2019-07-01' and fohk.knh_first_order_date < '2019-08-01' then
		'f_new_knh'
	when (fohs.web_first_order_date > '2019-08-01' or fohs.web_first_order_date is null) and ci.last_interaction > '2016-08-01' then
		'f_still_knh'
	when fohs.web_first_order_date > fohk.knh_first_order_date and fohs.web_first_order_date < '2019-07-01' and ci.last_interaction > '2016-08-01' then
		'f_knh->web'
	when fohs.web_first_order_date > fohk.knh_first_order_date and fohs.web_first_order_date > '2019-07-01' and fohs.web_first_order_date < '2019-08-01' and ci.last_interaction > '2016-08-01' then
		'f_conv_knh->web'
	else 'other' end as f_status,
	count(kc.customer_id) as 'count'
	--kc.customer_id as customer_id
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where order_date < '2019-07-01'
		        ) 
		group by customer_number
		) iohs on iohs.customer_number = ci.customer_number
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        where order_date < '2019-07-01'
		        )
		group by customer_id
		) iohk on iohk.customer_id = kc.customer_id	
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where order_date < '2019-08-01'
		        ) 
		group by customer_number
		) fohs on fohs.customer_number = ci.customer_number
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        where order_date < '2019-08-01'
		        )
		group by customer_id
		) fohk on fohk.customer_id = kc.customer_id	--)	
group by 1,2
;

