select count(ci.customer_number), count(distinct ci.customer_number), --ci.customer_number, nl.customer_number, nl.subscription_date, nl.unsubscription_date, ci.order_cnt,
	case when (order_cnt > 0 and nl.subscription_date < '2019-07-01' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-07-01' and ci.last_interaction > '2016-07-01') then
	  	'i_buyer_subscriber'
	when order_cnt > 0 and ci.last_interaction > '2016-07-01' then
		'i_buyer'
	when nl.subscription_date < '2019-07-01' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-07-01' and ci.last_interaction > '2016-07-01' then 
		'i_subscribed'
	else 'other' end as 'i_status'
from user_bsevilla.v_users ci
left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
where ci.leading_system in ('WEB', 'EVI')
group by 3
limit 100;


select *
from user_bsevilla.v_users ci 
left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
where nl.customer_number is null
limit 10
;

select * from user_bsevilla.v_users where lifecycle_status = 'Visitor'
--and last_order_date is null
--and last_open_date_newsletter is null
;



select leading_system, count(*) from zone_country_es.v_c_dama_ci where leading_system = 'ENL' group by 1;
