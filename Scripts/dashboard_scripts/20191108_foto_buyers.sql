select 
	'WEB' as leading_system,
	case when ci.order_rec_all <= 365 then '1year'
		 when ci.order_rec_all <= 730 then '2year'
		 else '3year' end as last_order_year,
	count(distinct ci.customer_number) as 'count'
from zone_country_es.v_dc_dama_ci ci
join zone_country_es.v_dc_order oh on ci.customer_number = oh.customer_number
join zone_country_es.v_dc_reco_content rc on ci.customer_number = rc.customer_number
join zone_country_es.v_dc_reco_availability ra on ci.customer_number = ra.customer_number and rc.RECO_MODULE_ID = ra.RECO_MODULE_ID
join zone_country_es.v_dc_lnk__customer__newsletter cn on ci.customer_number = cn.customer_number
join zone_country_es.v_dc_artist ab on rc.arid_1 = ab.artist_id
where 1=1
and ci.leading_system in ('EVI','WEB')
--and ci.customer_config_id = 27
and ci.order_rec_all <= 1095
and cn.newsletter_category_id in (5)
and cn.newsletter_status = 1
and ab.name is not null
and rc.arid_1 is not null
and rc.reco_module_id = 8
and oh.order_status = 100
and ci.customer_number not in ( -- call center filter (customers who ever ordered via cc)
	select 
		customer_number
	from zone_country_es.v_dc_order oh
	join zone_country_es.v_dc_affiliate a on a.affiliate_id = oh.affiliate_id
	where affiliate_short_name in ('CSV','YUI','CS0','CES','QQQ')
	and oh.order_status = 100
)
--and ci.CUSTOMER_AFFILIATE not in ('CSV','YUI','CS0','CES','QQQ')
and ci.customer_number not in ( -- blacklist filter
	select 
		customer_number 
	from zone_country_es.v_dc_lnk__customer__blacklist
)
and ci.customer_number not in ( -- ordered artist filter
	select 
		customer_number
	from zone_country_es.v_dc_lnk__customer__artist
	where artist_id = 514210
	and order_cnt_all > 0
)
group by 1,2
union
select 
	'KNH' as leading_system,
	case when ci.order_rec_all <= 365 then '1year'
		 when ci.order_rec_all <= 730 then '2year'
		 else '3year' end as last_order_year,
	count(distinct ci.customer_number) as 'count'
from zone_country_es.v_dc_dama_ci ci
where 1=1
and ci.leading_system = 'KNH'
--and ci.customer_config_id = 27
and ci.order_rec_all <= 1095
--and ci.CUSTOMER_AFFILIATE not in ('CSV','YUI','CS0','CES','QQQ')
and ci.customer_number not in ( -- blacklist filter
	select 
		customer_number 
	from zone_country_es.v_dc_lnk__customer__blacklist
)
and ci.customer_number not in ( -- ordered artist filter
	select 
		customer_number
	from zone_country_es.v_dc_lnk__customer__artist
	where artist_id = 514210
	and order_cnt_all > 0
)
group by 1,2
;