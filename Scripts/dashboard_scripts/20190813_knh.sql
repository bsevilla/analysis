
--select count(customer_id), count(distinct customer_id) from (
select 
	--count(kc.customer_id) as 'count'
	ci.customer_number as customer_number,
	ohs.web_first_order_date,
	ohk.knh_first_order_date
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where order_date < '2019-07-01'
		        ) 
		group by customer_number
		) ohs on ohs.customer_number = ci.customer_number
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        )
		group by customer_id
		) ohk on ohk.customer_id = kc.customer_id
left join 
		(select distinct kundennummer, kundenemail
		 from zone_country_es.v_c_tcdb__dbo__order_header
		 ) oh on oh.kundennummer = ci.customer_number
where ohs.web_first_order_date > ohk.knh_first_order_date and ci.last_interaction > '2016-08-21'
limit 100
;



--------------------------------

--     VERSIONES PASADAS      --

--------------------------------



select distinct er.id as erid, hk.hauptkategorieid as main_category_id, hk.bezeichnung as main_category_name
from zone_country_es.v_c_tcdb__dbo__eventreihe as er
join zone_country_es.v_c_tcdb__dbo__event as ev on ev.er_id = er.id
join zone_country_es.v_c_tdl__comb__vorstellung as v on v.vorstellungid = ev.tdl_vorstellung_id
join zone_country_es.v_c_tdl__comb__veranstaltung as ver on ver.veranstaltungid = v.veranstaltungid
join zone_country_es.v_c_tdl__comb__kategorie as k on k.kategorieid = ver.kategorieid
join zone_country_es.v_c_tdl__comb__hauptkategorie as hk on hk.hauptkategorieid = k.hauptkategorieid
limit 10;



select distinct
	oh.kundennummer, 
	kc.customer_id, 
	ci.first_order_date as first_web_order, 
	fk.first_kino_order,
	cnt.order_cnt
from zone_country_es.v_c_kinoheld__dim_customer kc
inner join zone_country_es.v_c_tcdb__dbo__order_header oh on lower(oh.kundenemail) = lower(kc.customer_email)
left join zone_country_es.v_c_dama_ci ci on oh.kundennummer = ci.customer_number
/*inner join (select ci.customer_number, 
			case when ci.registration_date >= ci.first_order_date then ci.first_order_date else ci.registration_Date end as first_thing
			from zone_country_es.v_c_dama_ci ci) ft on ft.customer_number = ci.customer_number*/
join (select oh.customer_id as customer_id, count(distinct oh.order_number) as order_cnt
			from zone_country_es.v_c_kinoheld__dim_customer dc 
			left join zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
			left join zone_country_es.v_c_dama_ci ci on ci.customer_email = dc.pii_customer_email
			where oh.order_date < ci.first_order_date
			group by 1) cnt on cnt.customer_id = kc.customer_id
join (select customer_id, min(order_date) as first_kino_order 
	  from zone_country_es.v_c_kinoheld__fact_order
	  group by 1
) fk on kc.customer_id = fk.customer_id
where fk.first_kino_order < ci.first_order_date
;

select distinct
	oh.kundennummer, 
	kc.customer_id, 
	ci.first_order_date as first_web_order, 
	fk.first_kino_order,
	cnt.order_cnt,
	days_between(ci.first_order_date,lo.last_kino_order) as diff_orders
from zone_country_es.v_c_kinoheld__dim_customer kc
inner join zone_country_es.v_c_tcdb__dbo__order_header oh on lower(oh.kundenemail) = lower(kc.customer_email)
left join zone_country_es.v_c_dama_ci ci on oh.kundennummer = ci.customer_number
join (select oh.customer_id as customer_id, count(distinct oh.order_number) as order_cnt
			from zone_country_es.v_c_kinoheld__dim_customer dc 
			left join zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
			left join zone_country_es.v_c_dama_ci ci on ci.customer_email = dc.pii_customer_email
			where oh.order_date < ci.first_order_date
			group by 1) cnt on cnt.customer_id = kc.customer_id
join (select customer_id, min(order_date) as first_kino_order 
	  from zone_country_es.v_c_kinoheld__fact_order
	  group by 1
) fk on kc.customer_id = fk.customer_id
join (select dc.customer_id as customer_id, max(oh.order_date) as last_kino_order
	  from zone_country_es.v_c_kinoheld__dim_customer dc 
	  left join zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
	  left join zone_country_es.v_c_dama_ci ci on ci.customer_email = dc.pii_customer_email
	  where oh.order_date < ci.first_order_date
	  group by 1
) lo on lo.customer_id = kc.customer_id
where fk.first_kino_order < ci.first_order_date
;

select avg(order_cnt_all)
from zone_country_es.v_c_dama_ci
where leading_system = 'KNH';
