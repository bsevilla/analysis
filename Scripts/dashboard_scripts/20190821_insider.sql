-- insider receivers

-- cuenta basica

select 
	reco_level,
	count(distinct ci.customer_number) as 'count'
from zone_country_es.v_dc_dama_ci ci
join zone_country_es.v_dc_order oh on ci.customer_number = oh.customer_number
join zone_country_es.v_dc_reco_content rc on ci.customer_number = rc.customer_number
join zone_country_es.v_dc_reco_availability ra on ci.customer_number = ra.customer_number and rc.RECO_MODULE_ID = ra.RECO_MODULE_ID
join zone_country_es.v_dc_lnk__customer__newsletter cn on ci.customer_number = cn.customer_number
join zone_country_es.v_dc_artist ab on rc.arid_1 = ab.artist_id
where 1=1
and ci.leading_system in ('EVI','WEB')
--and ci.customer_config_id = 27
and ci.order_rec_all <= 1095
and cn.newsletter_category_id in (5)
and cn.newsletter_status = 1
and ab.name is not null
and rc.arid_1 is not null
and rc.reco_module_id = 8
and oh.order_status = 100
and ci.customer_number not in ( -- call center filter (customers who ever ordered via cc)
	select 
		customer_number
	from zone_country_es.v_dc_order oh
	join zone_country_es.v_dc_affiliate a on a.affiliate_id = oh.affiliate_id
	where affiliate_short_name in ('CSV','YUI','CS0','CES','QQQ')
	and oh.order_status = 100
)
--and ci.CUSTOMER_AFFILIATE not in ('CSV','YUI','CS0','CES','QQQ')
and ci.customer_number not in ( -- blacklist filter
	select 
		customer_number 
	from zone_country_es.v_dc_lnk__customer__blacklist
)
and ci.customer_number not in ( -- ordered artist filter
	select 
		customer_number
	from zone_country_es.v_dc_lnk__customer__artist
	where artist_id = 514210
	and order_cnt_all > 0
)
group by 1 order by 1
;

-- cuentas

select distinct nl.customer_number, ins.reco_level, nl.newsletter_type, nl.newsletter_category--, count(distinct nl.customer_number)
from user_bsevilla.v_newsletters nl
inner join (
			select distinct
				reco_level,
				ci.customer_number as customer_number
			from zone_country_es.v_dc_c_dama_ci_base ci
			join zone_country_es.v_dc_fact_order_base oh on ci.customer_number = oh.customer_number
			join zone_country_es.v_dc_fact_reco_content_base rc on ci.customer_number = rc.customer_number
			join zone_country_es.v_dc_fact_reco_availability_base ra on ci.customer_number = ra.customer_number and rc.RECO_MODULE_ID = ra.RECO_MODULE_ID
			join zone_country_es.v_dc_c_bridge_customer_newsletter_base cn on ci.customer_number = cn.customer_number
			join zone_country_es.v_dc_c_dim_artist_base ab on rc.arid_1 = ab.artist_id
			where 1=1
			and ci.leading_system in ('EVI','WEB')
			--and ci.customer_config_id = 27
			and ci.order_rec_all <= 1095
			and cn.newsletter_category_id in (5)
			and cn.newsletter_status = 1
			and ab.name is not null
			and rc.arid_1 is not null
			and rc.reco_module_id = 8
			and oh.order_status = 100
			and ci.customer_number not in ( -- call center filter (customers who ever ordered via cc)
				select 
					customer_number
				from zone_country_es.v_dc_fact_order_base oh
				join zone_country_es.v_dc_c_dim_affiliate_base a on a.affiliate_id = oh.affiliate_id
				where affiliate_short_name in ('CSV','YUI','CS0','CES','QQQ')
				and oh.order_status = 100
			)
			--and ci.CUSTOMER_AFFILIATE not in ('CSV','YUI','CS0','CES','QQQ')
			and ci.customer_number not in ( -- blacklist filter
				select 
					customer_number 
				from zone_country_es.v_dc_c_bridge_customer_blacklist_base
			)
			and ci.customer_number not in ( -- ordered artist filter
				select 
					customer_number
				from zone_country_es.v_dc_c_bridge_customer_artist_base
				where artist_id = 514210
				and order_cnt_all > 0
			)
			) ins on ins.customer_number = nl.customer_number
--group by 1,2,3
--order by 1,2,3
;


