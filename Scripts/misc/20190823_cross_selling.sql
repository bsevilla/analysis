-- cross selling

select  ci.customer_number, 
		ohs.order_1, 
		ohs.vert_1, 
		days_between(ohs.order_2, ohs.order_1) as day_order_2,
		ohs.vert_2, 
		days_between(ohs.order_3, ohs.order_2) as day_order_3,
		ohs.vert_3
from user_bsevilla.v_users ci
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as order_1, 
		        max(case when rnAsc = 1 then vertical end) as vert_1,
		        max(case when rnAsc = 2 then order_date end) as order_2,
		        max(case when rnAsc = 2 then vertical end) as vert_2,
		        max(case when rnAsc = 3 then order_date end) as order_3,
		        max(case when rnAsc = 3 then vertical end) as vert_3
		from    (
		        select  customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date, vertical
		        from user_bsevilla.v_orders
		        )
		group by
		        customer_number
		) ohs on ohs.customer_number = ci.customer_number
where ci.leading_system in ('WEB','EVI')
and ci.last_interaction > '2016-08-23'
limit 10
;     