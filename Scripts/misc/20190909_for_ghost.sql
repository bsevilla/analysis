-- users with > 10 orders per year



select distinct u.customer_number, em.kundenemail, ci.monetary_value_lng, u.order_cnt, u.reco_level, u.entry_product, u.federal_region
from user_bsevilla.v_users u
right join 
			(
				select customer_number, count(order_number) as order_cnt
				from user_bsevilla.v_orders
				where order_date > '2018-09-16'
				and artist_id not in (551894, 500456, 535946, 499703, 551929,561591,493380)
				group by 1
				having count(order_number) > 10
			) o on o.customer_number = u.customer_number
left join zone_country_es.v_c_dama_ci ci on ci.customer_number = u.customer_number
left join (
				select distinct kundennummer, kundenemail
				from zone_country_es.v_c_tcdb__dbo__order_header
		   )  em on em.kundennummer = u.customer_number
left join (
			select customer_number, max(ord_cnt) as max_cnt
			from (
					select customer_number, erid, count(distinct order_number) as ord_cnt
					from user_bsevilla.v_orders
					group by 1, 2
			) 
			group by 1
) co on co.customer_number = u.customer_number
/*inner join (
			select distinct customer_number 
			from user_bsevilla.v_newsletters
			where newsletter_type = 'General NL'
		  ) nl on u.customer_number = nl.customer_number*/
left join zone_country_es.v_dc_fact_order_base oh on ci.customer_number = oh.customer_number
left join zone_country_es.v_dc_fact_reco_content_base rc on ci.customer_number = rc.customer_number
left join zone_country_es.v_dc_fact_reco_availability_base ra on ci.customer_number = ra.customer_number and rc.reco_module_id = ra.reco_module_id
left join zone_country_es.v_dc_c_bridge_customer_newsletter_base cn on ci.customer_number = cn.customer_number
left join zone_country_es.v_dc_c_dim_artist_base ab on rc.arid_1 = ab.artist_id
where 1=1
and cn.newsletter_category_id in (5)
and cn.newsletter_status = 1
and ab.name is not null
and rc.arid_1 is not null
and rc.reco_module_id = 8
and oh.order_status = 100
and u.suffix in ('gmail.com','hotmail.com','yahoo.es','hotmail.es','telefonica.net','yahoo.com','outlook.com','me.com','icloud.com','outlook.es')
and co.max_cnt <= 2
and u.customer_number not in (950383337)
and u.reco_level = 3
and not exists 
			(
				select *
				from user_bsevilla.v_orders as o2
				where o.customer_number = o2.customer_number
				and (artist_id = 510873
				or vertical = 'Musical')
			)
and ci.membervoucher_status <> 1
and em.kundenemail is not null
and u.zip_code like '28%'
order by monetary_value_lng desc
limit 30
;


-- fanbonus
select distinct ci.customer_number, em.kundenemail, membervoucher_value, ci.reco_level, order_cnt_all, u.entry_product
from zone_country_es.v_c_dama_ci ci
left join (
				select distinct kundennummer, kundenemail
				from zone_country_es.v_c_tcdb__dbo__order_header
		   )  em on em.kundennummer = ci.customer_number
left join user_bsevilla.v_users u on u.customer_number = ci.customer_number
left join (
			select customer_number, max(ord_cnt) as max_cnt
			from (
					select customer_number, erid, count(distinct order_number) as ord_cnt
					from user_bsevilla.v_orders
					group by 1, 2
			) 
			group by 1
) co on co.customer_number = ci.customer_number
inner join (
			select distinct customer_number 
			from user_bsevilla.v_newsletters
			where newsletter_type = 'General NL'
		  ) nl on ci.customer_number = nl.customer_number
left join zone_country_es.v_dc_fact_order_base oh on ci.customer_number = oh.customer_number
left join zone_country_es.v_dc_fact_reco_content_base rc on ci.customer_number = rc.customer_number
left join zone_country_es.v_dc_fact_reco_availability_base ra on ci.customer_number = ra.customer_number and rc.reco_module_id = ra.reco_module_id
left join zone_country_es.v_dc_c_bridge_customer_newsletter_base cn on ci.customer_number = cn.customer_number
left join zone_country_es.v_dc_c_dim_artist_base ab on rc.arid_1 = ab.artist_id
where 1=1
and cn.newsletter_category_id in (5)
and cn.newsletter_status = 1
and ab.name is not null
and rc.arid_1 is not null
and rc.reco_module_id = 8
and oh.order_status = 100
and ci.membervoucher_status = 1
and co.max_cnt <= 2
and ci.membervoucher_value is not null
and ci.reco_level > 0
and ci.customer_email_suffix in ('gmail.com','hotmail.com','yahoo.es','hotmail.es','telefonica.net','yahoo.com','outlook.com','me.com','icloud.com','outlook.es')
and ci.customer_number not in  
			(
				select distinct customer_number
				from user_bsevilla.v_orders
				where (artist_id = 510873
				or vertical = 'Musical')
			)
and kundenemail is not null
and u.zip_code like '28%'
order by 3 desc
limit 30
;


select kundennummer, kundennachname, kundenvorname
from zone_country_es.v_c_tcdb__dbo__kunde
where kundennummer in (95205570,952326854,952189193,952101667,952040008,950235057,952225014,950157973,950401153,950554251,952472471,950084640,950793608,952240325)
;

select kundennummer, kundennachname, kundenvorname
from zone_country_es.v_c_tcdb__dbo__kunde
where kundennummer in (950367982,952623860,952655114,952547525,952577515,952557843,951493959,952651214,952651273,952502699,952489169,952672031,950051873,952456342,952572686,952477018,952593185,952752652,952548539)
;


