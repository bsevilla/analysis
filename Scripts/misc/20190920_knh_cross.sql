-- knh pass

select distinct ci.customer_number, ci.customer_id, ci.pii_customer_email
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join zone_country_es.v_c_dama_ci ci2 on ci2.customer_number = ci.customer_number
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where origin_info in ('WEB','EVI')
		        ) 
		group by customer_number
		) iohs on iohs.customer_number = ci.customer_number
left join 
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        )
		group by customer_id
		) iohk on iohk.customer_id = kc.customer_id	
left join zone_country_es.v_fact_order_base oh on cast(oh.order_number as varchar(20))= cast(ci.first_order_number as varchar(20))
where iohs.web_first_order_date > iohk.knh_first_order_date 
and ci.last_interaction > '2016-10-03'
and ci.first_event_erid is not null
and ci.honesty_status > 0
limit 100
;

select * from user_bsevilla.v_newsletters where newsletter_type is not null limit 10;

select
                c.leading_system
                ,o.source_name
                ,count(distinct c.customer_id) as customers
                ,count(distinct o.order_id) as orders
from zone_country_es.v_c_dama_ci c
left join zone_country_es.v_fact_order_base o on o.customer_id = c.customer_id
                and o.order_status > 0
group by 1,2
order by 1,2
;

select order_date
from zone_country_es.v_fact_order_base limit 10;

select count(order_id) from zone_country_es.v_fact_order_mailing;

select * from zone_country_es.v_fact_order_mailing limit 10;


select cl.venue_city, venue_name, cp.provincia from user_bsevilla.v_dim_cinema_locations cl
left join user_bsevilla.v_dim_cp cp on cp.cp = cl.venue_postal_code;


