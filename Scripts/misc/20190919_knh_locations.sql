-- knh locations

-- LOCATION
select c.provincia as provincia, count(distinct dc.customer_id) as cnt, count(distinct dc.customer_id)/max(poblacion) as pct_pob
from zone_country_es.v_c_kinoheld__dim_customer dc
left join zone_country_es.v_c_kinoheld__fact_order oh on oh.customer_id = dc.customer_id
left join zone_country_es.v_c_kinoheld__fact_order_position op on op.order_id = oh.order_id
left join zone_country_es.v_c_kinoheld__dim_event de on de.event_id = op.event_id
left join zone_country_es.v_c_kinoheld__dim_venue dv on dv.venue_id = de.venue_id
left join user_bsevilla.v_dim_cp c on dv.venue_postal_code = c.cp
where oh.order_date > '2018-09-19'
and lower(dc.customer_email) not like '%entradas%'
group by 1
order by 2 desc
;

-- GENRE
select dg.genre_name, de.event_name1, count(distinct dc.customer_id)
from zone_country_es.v_c_kinoheld__dim_customer dc
left join zone_country_es.v_c_kinoheld__fact_order oh on oh.customer_id = dc.customer_id
left join zone_country_es.v_c_kinoheld__fact_order_position op on op.order_id = oh.order_id
left join zone_country_es.v_c_kinoheld__dim_event de on de.event_id = op.event_id
left join zone_country_es.v_c_kinoheld__dim_venue dv on dv.venue_id = de.venue_id
left join user_bsevilla.v_dim_cp c on dv.venue_postal_code = c.cp
left join zone_country_es.v_c_kinoheld__link_event_artist ea on op.event_id = ea.event_id
left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ea.artist_id = ag.artist_id
left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
where oh.order_date > '2018-09-19'
and lower(dc.customer_email) not like '%entradas%'
and c.provincia = 'Madrid'
group by 1,2
order by 3 desc
;

-- CROSS SELLER LOCATION
select c.provincia as provincia_knh, c2.provincia as provincia_web, count(distinct dc.customer_id) as cnt
from zone_country_es.v_c_kinoheld__dim_customer dc
left join zone_country_es.v_c_kinoheld__fact_order oh on oh.customer_id = dc.customer_id
left join zone_country_es.v_c_kinoheld__fact_order_position op on op.order_id = oh.order_id
left join zone_country_es.v_c_kinoheld__dim_event de on de.event_id = op.event_id
left join zone_country_es.v_c_kinoheld__dim_venue dv on dv.venue_id = de.venue_id
left join user_bsevilla.v_dim_cp c on dv.venue_postal_code = c.cp
left join user_bsevilla.v_users u on u.pii_customer_email = dc.pii_customer_email
left join zone_country_es.v_c_dama_event ev on ev.event_id = u.first_event_evid
left join user_bsevilla.v_dim_cp c2 on ev.venue_zip = c2.cp
inner join (
			select kc.customer_id as customer_id
			from zone_country_es.v_c_kinoheld__dim_customer kc
			left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
			left join 
					(select  customer_number,
					        max(case when rnAsc = 1 then order_date end) as web_first_order_date
					from    (
					        select  customer_number as customer_number,
					                row_number() over (partition by customer_number order by order_date) as rnAsc, 
					                order_date
					        from user_bsevilla.v_orders
					        ) 
					group by customer_number
					) iohs on iohs.customer_number = ci.customer_number
			left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
					(select  customer_id,
					        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
					from    (
					        select  customer_id,
					                row_number() over (partition by customer_id order by order_date) as rnAsc, 
					                order_date
					        from zone_country_es.v_c_kinoheld__fact_order
					        )
					group by customer_id
					) iohk on iohk.customer_id = kc.customer_id		
			where iohs.web_first_order_date > iohk.knh_first_order_date 
			and ci.last_interaction > '2016-09-20'
			) kc on kc.customer_id = dc.customer_id
where oh.order_date > '2018-09-19'
and lower(dc.customer_email) not like '%entradas%'
group by 1,2
order by 3 desc
;

select * from zone_country_es.v_c_dama_event limit 10;
