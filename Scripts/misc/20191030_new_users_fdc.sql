select cp.provincia, count(distinct ci.customer_id)
from zone_country_es.v_c_kinoheld__dim_customer ci
left join zone_country_es.v_c_kinoheld__fact_order oh on oh.customer_id = ci.customer_id
left join zone_country_es.v_c_kinoheld__fact_order_position op on op.order_id = oh.order_id
left join zone_country_es.v_c_kinoheld__dim_event ev on ev.event_id = op.event_id
left join zone_country_es.v_c_kinoheld__dim_venue dv on dv.venue_id = ev.venue_id
left join user_bsevilla.v_dim_cp cp on cp.cp = dv.venue_postal_code
where ci.customer_registration_date > '2019-10-26'
and ci.customer_registration_date < '2019-10-31'
group by 1
order by 2 desc
;


select dv.venue_name, dv.venue_postal_code, dv.venue_city, count(u.kundennummer)
from zone_country_es.v_c_kinoheld__dim_venue dv
left join user_bsevilla.v_dim_cp cp1 on cp1.cp = dv.venue_postal_code
left join (select k.kundennummer, cp2.provincia
		from zone_country_es.v_c_tcdb__dbo__kunde k
		left join user_bsevilla.v_dim_cp cp2 on cp2.cp = k.kundenplz
		) u on u.provincia = cp1.provincia
group by 1,2,3
order by 4 desc

select * from zone_country_es.v_c_kinoheld__dim_event limit 10;

