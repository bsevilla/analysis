-- van gogh

--select event_date_local, event_id, event_name, data_source 
select event_date_local as event_date
from zone_country_es.v_c_dama_event
where event_series_id in (44810100005819, 44857900005819, 44809900005819)
order by 1
;

select count(distinct oh.order_id), count(distinct ci.customer_number)
from zone_country_es.t_legacy_test__dim_customer ci
left join zone_country_es.t_legacy_test__fact_order oh on oh.customer_number = ci.customer_number
left join zone_country_es.t_legacy_test__fact_order_position op on oh.order_id = op.order_id
left join zone_country_es.t_legacy_test__dim_event ev on ev.event_id = op.event_id
where ev.eventseries_id in (56984, 56986, 56895)
;
--56984, 56986, 56895
--44810100005819, 44857900005819, 44809900005819
select * 
from zone_country_es.enl
where ev.eventseries_id in (56984, 56986, 56895)
limit 10;

select to_char(oh.order_date, 'YYYYMMDD') as order_date, count(distinct oh.order_id)
from zone_country_es.v_enl__fact_order oh
left join zone_country_es.v_enl__fact_order_position op on oh.order_id = op.order_id
left join zone_country_es.v_c_dama_event ev on op.event_id = ev.event_id
left join zone_country_es.v_fact_order_base ob on ob.event_id = ev.event_id
where ev.event_series_id in (44810100005819, 44857900005819, 44809900005819)
group by 1
order by 1
;

select ev.event_date_local as event_date, count(distinct oh.order_id) as cnt
from zone_country_es.v_enl__fact_order oh
left join zone_country_es.v_enl__fact_order_position op on oh.order_id = op.order_id
left join zone_country_es.v_c_dama_event ev on op.event_id = ev.event_id
left join zone_country_es.v_fact_order_base ob on ob.event_id = ev.event_id
where ev.event_series_id in (44810100005819, 44857900005819, 44809900005819)
group by 1
order by 1
;

select case when ev.event_name like 'VAN GOGH%' then 'VAN GOGH' else ev.event_name end as event_name, 
	   count(distinct ob.order_number)
from zone_country_es.v_fact_order_base ob
right join (
			select distinct ob.customer_number
			from zone_country_es.v_enl__fact_order oh
			left join zone_country_es.v_enl__fact_order_position op on oh.order_id = op.order_id
			left join zone_country_es.v_c_dama_event ev on op.event_id = ev.event_id
			left join zone_country_es.v_fact_order_base ob on ob.event_id = ev.event_id
			where ev.event_series_id in (44810100005819, 44857900005819, 44809900005819)
			) fil on fil.customer_number = ob.customer_number
left join zone_country_es.v_c_dama_event ev on ob.event_id = ev.event_id
group by 1
order by 2 desc
;


select distinct ev.event_name, v.venue_name, ev.event_series_name, ev.event_series_id
from zone_country_es.v_c_dama_event ev
left join zone_country_es.v_c_dama_venue v on ev.venue_id = v.venue_id
where main_artist_id = 565654;

select distinct artist_name, artist_id
from zone_country_es.v_c_dama_artist where lower(artist_name) like '%van gogh%'




select count(distinct oh.ordernummer), count(distinct oh.kundennummer), sum(number_of_tickets)
from zone_country_es.v_c_tcdb__dbo__order_header oh
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.ordernummer = op.ordernummer
where op.er_id = 2403211
;

select to_char(oh.orderdatum, 'YYYYMMDD') as order_date, count(distinct oh.ordernummer)
from zone_country_es.v_c_tcdb__dbo__order_header oh
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.ordernummer = op.ordernummer
where op.er_id = 2403211
group by 1
order by 1
;

select to_char(oh.orderdatum, 'HH:mm') as order_date--, count(distinct oh.ordernummer)
from zone_country_es.v_c_tcdb__dbo__order_header oh
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.ordernummer = op.ordernummer
where op.er_id = 2403211
--group by 1
--order by 1
;

select * from 
zone_country_es.v_c_tcdb__dbo__order_position op
limit 10;


select ev.event_series_name, 
	   count(distinct ob.order_number)
from zone_country_es.v_fact_order_base ob
right join (
			select distinct oh.kundennummer as customer_number
			from zone_country_es.v_c_tcdb__dbo__order_header oh
			left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.ordernummer = op.ordernummer
			where op.er_id = 2403211
			) fil on fil.customer_number = ob.customer_number
left join zone_country_es.v_c_dama_event ev on ob.event_id = ev.event_id
group by 1
order by 2 desc
;

select ev.event_date_local as event_date, count(distinct oh.ordernummer) as cnt
from zone_country_es.v_c_tcdb__dbo__order_header oh
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.ordernummer = op.ordernummer
left join zone_country_es.v_c_dama_event ev on op.ev_id = ev.event_id
where op.er_id = 2403211
group by 1
order by 1

select 100*count(distinct u.customer_number)/1087
from user_bsevilla.v_users u
where u.first_event_erid = 2403211;

select *
from user_bsevilla.v_users limit 10;