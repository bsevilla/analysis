-- prueba one time buyers
select federal_region, count(customer_number)/291400
from user_bsevilla.v_users
where leading_system in ('WEB', 'EVI')
and last_interaction > '2016-08-28'
and last_order_date > '2018-08-28'
and order_cnt = 1
group by 1
order by 2 desc
;

-- artist name
select ku.name, count(ci.customer_number)
from user_bsevilla.v_users ci
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link ek on ek.er_id = ci.first_event_erid
left join zone_country_es.v_c_tcdb__dbo__kuenstler ku on ek.ku_id = ku.id
where ci.leading_system in ('WEB', 'EVI')
and ci.last_interaction > '2016-08-28'
and ci.last_order_date > '2019-05-28'
and ci.order_cnt = 1
group by 1
order by 2 desc
limit 100
;

-- subcategory
select k.name, ku.name, count(ci.customer_number)
from user_bsevilla.v_users ci
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kategorie_link ek on ek.er_id = ci.first_event_erid
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kategorie k on ek.erk_id = k.id
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link eku on eku.er_id = ci.first_event_erid
left join zone_country_es.v_c_tcdb__dbo__kuenstler ku on eku.ku_id = ku.id
where ci.leading_system in ('WEB', 'EVI')
and ci.last_interaction > '2016-08-28'
and ci.last_order_date > '2019-05-28'
and ci.order_cnt = 1
group by 1,2
order by 3 desc
limit 100
;

select da.artist_id, g.genre_name --, da.artist_name
from zone_country_es.v_c_kinoheld__link_artist_genre ag
left join zone_country_es.v_c_dama_artist da on da.source_artist_id = ag.artist_id
left join zone_country_es.v_c_kinoheld__dim_genre g on g.genre_id = ag.genre_id
where ag.genre_id in (416,162,256,166,396,408)
;

select id, name, last_update
from zone_country_es.v_c_tcdb__dbo__eventreihe where name like '%Le�n%';

select artist_name, artist_id, vertical,
	   avg(order_total/ticket_number) 
from user_bsevilla.v_orders 
where order_date > '2019-02-28'
group by 1,2,3
having avg(order_total/ticket_number) < 50
and vertical = 'Musical'
order by 4 desc
;

-- results

select * from zone_country_es.v_c_dama_csr
where mailing_name = 'AGE_MUSL_20190904_GRANDE TGP_3_T3ED_ONETIMEBUYERS_20190904_MUSICAL_GRANDE'
;

select u.leading_system, count(u.customer_number)
from zone_country_es.v_c_optf__comb__mailingrecipients r
left join zone_country_es.v_c_dama_csr csr on csr.mailing_id = r.mailingid
left join zone_country_es.v_c_dama_ci u on u.customer_email = pii_userid
where csr.mailing_name = 'AGE_OTRO_20190910_MIX TGP_1_T1ED_CROSS_COMEDIA_LGBT_lgbt'
and u.last_order_date > '2019-09-10'
group by 1;

select u.leading_system, count(u.customer_number)
from zone_country_es.v_c_optf__comb__mailingrecipients r
left join zone_country_es.v_c_dama_csr csr on csr.mailing_id = r.mailingid
left join zone_country_es.v_c_dama_ci u on u.customer_email = pii_userid
where csr.mailing_name = 'AGE_TEAT_20190910_TEATRO_COMEDIA TGP_2_T1ED_CROSS_COMEDIA_LGBT_comedia'
and u.last_order_date > '2019-09-10'
group by 1;



select csr.mailing_name, u.order_cnt_all, count(u.customer_number)
from zone_country_es.v_c_optf__comb__mailingrecipients r
left join zone_country_es.v_c_dama_csr csr on csr.mailing_id = r.mailingid
left join zone_country_es.v_c_dama_ci u on u.customer_email = pii_userid
where csr.mailing_name in (('AGE_MUSL_20190904_GRANDE TGP_3_T3ED_ONETIMEBUYERS_20190904_MUSICAL_GRANDE',
'AGE_MUSL_20190904_PEQUE�O TGP_4_T3ED_ONETIMEBUYERS_20190904_MUSICAL_PEQUE�O',
'AGE_TEAT_20190904_COMEDIA TGP_1_T3ED_ONETIMEBUYERS_20190904_COMEDIA',
'AGE_TEAT_20190904_DRAMA TGP_2_T3ED_ONETIMEBUYERS_20190904_DRAMA')
)
and u.last_order_date > '2019-09-04'
and order_cnt_all < 20
group by 1,2;


select count(u.customer_number)
from user_bsevilla.v_users u 
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-08-28'
		group by 1
		) ioh on ioh.customer_number = u.customer_number
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-09-03'
		group by 1
		) foh on foh.customer_number = u.customer_number		
where u.leading_system in ('WEB','EVI')
and u.last_interaction > '2016-08-28'
and ioh.order_cnt = 1
--and foh.order_cnt > 1
;



