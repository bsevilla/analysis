select artist_name, artist_id
from zone_country_es.v_c_dama_artist
where 1=1
and data_source = 'KNH'
and (lower(artist_name) like '%cementerio%'
--	or artist_name like '%V.O%'
--  or lower(artist_name) like '%sue�o%'
--  or lower(artist_name) like '%us%'
--  or lower(artist_name) like '%peque�os%'
)
--and first_order_date > '2018-10-16'
order by 2
limit 1050;

select artist_name, artist_id, order_rec_all
from zone_country_es.v_c_dama_artist
where 1=1
and data_source = 'WEB'
and (lower(artist_name) like '%hole%'
  or lower(artist_name) like '%apocalipsis%'
)
order by 3 desc
;

select er.name, er.id
from zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link ekl
left join zone_country_es.v_c_tcdb__dbo__eventreihe er on ekl.er_id = er.id
where ku_id = 561313;

select distinct artist_name, artist_id
from zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link eku
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kategorie_link ekl on eku.er_id = ekl.er_id
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kategorie eka on eka.id = ekl.erk_id
left join zone_country_es.v_c_dama_artist da on da.artist_id = eku.ku_id
where eka.name like '%Hip-hop%';


select max(order_date) 
from zone_country_es.v_fact_order_base
where artist_id = 493380
;


select distinct customer_city_name
from zone_country_es.v_dc_dama_ci
where (lower(customer_city_name) like '%barcelona%'
or lower(customer_city_name) like '%barna%'
or lower(customer_city_name) like '%bcn%')
;

select artist_id, count(distinct order_number) 
from zone_country_es.v_fact_order_base
where 1=1
and artist_id in (555880)
group by 1;

select a.artist_name, a.artist_id, count(distinct ob.order_number)
from zone_country_es.v_c_dama_artist a
left join zone_country_es.v_fact_order_base ob on a.artist_id = ob.artist_id
where 1=1
and data_source = 'KNH'
and ob.venue_zip like '08___'
and first_order_date > '2018-10-16'
group by 1,2
order by 3 desc
;

select *
from zone_country_es.v_c_kinoheld__dim_artist
where artist_name like 'Dumbo'
limit 10
;
1260600010319

select * from zone_country_es.v_c_kinoheld__link_artist_genre where artist_id = 12606;
select * from zone_country_es.v_c_kinoheld__dim_genre where genre_id = 172;

select distinct to_char(ev.datum, 'YYYY-MM-DD') as event_date, ev.name1 as event, v.ort as city, artist_id as kuid
from zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link eku
left join zone_country_es.v_c_tcdb__dbo__eventreihe er on er.id = eku.er_id
left join zone_country_es.v_c_tcdb__dbo__event ev on ev.er_id = er.id
left join zone_country_es.v_c_tcdb__dbo__venue v on v.id = ev.ve_id
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kategorie_link ekl on eku.er_id = ekl.er_id
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kategorie eka on eka.id = ekl.erk_id
left join zone_country_es.v_c_tcdb__dbo__haupt_kategorie hk on hk.id = eka.hk_id 
left join zone_country_es.v_c_dama_artist da on da.artist_id = eku.ku_id
where hk.name like '%Concierto%'
and ev.datum > '2019-11-11' and ev.datum < '2019-11-25'
order by 1,3,2
;

select * from zone_country_es.v_c_dama_venue where data_source = 'KNH' 
and lower(venue_name) like '%paz%'
limit 10;

select distinct o.evid
from user_bsevilla.v_orders o 
left join zone_country_es.v_c_dama_event ev on o.evid = ev.event_id
where artist_name like 'Anastasia%'
and ev.event_timestamp > '2019-11-01' and ev.event_timestamp < '2019-12-31'
;

select * from zone_country_es.v_c_dama_event limit 10;

select ev.id, main_artist_id, main_artist_name
from zone_country_es.v_c_tcdb__dbo__event ev 
left join zone_country_es.v_c_dama_event dev on ev.id = dev.event_id
where ev.er_id = 2602125;

select distinct event_name, main_artist_name, main_artist_id
from zone_country_es.v_c_dama_event
where event_name in (select event_name from zone_country_es.v_c_dama_event where main_artist_id = 575997)
and main_artist_name <> 'Navidad Monumental'
;