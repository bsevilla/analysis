select u.reco_level, count(u.customer_number)
from user_bsevilla.v_users u 
left join (
			select customer_number, count(order_number) as ords
			from user_bsevilla.v_orders
			group by 1
		  ) oe on oe.customer_number = u.customer_number
left join user_bsevilla.v_orders o on o.customer_number = u.customer_number	
where u.leading_system in ('WEB','EVI')
and u.last_interaction > '2016-08-28'
and u.suffix <> 'entradas.com'
and oe.ords = 1
group by 1
order by 1
;


-- ciudad del evento

select a.provincia, a.entry_product, a.cnt, a.cnt/b.tot
from		(select cp2.provincia provincia, u.entry_product entry_product, count(u.customer_number) cnt
			from user_bsevilla.v_users u 
			left join (
						select customer_number, count(order_number) as ords
						from user_bsevilla.v_orders
						group by 1
					  ) oe on oe.customer_number = u.customer_number
			left join user_bsevilla.v_orders o on o.customer_number = u.customer_number
			left join zone_country_es.v_c_tcdb__dbo__event e on e.id = o.evid
			left join zone_country_es.v_c_tcdb__dbo__venue v on e.ve_id = v.id 
			left join user_bsevilla.v_dim_cp cp on v.plz = cp.cp
			left join user_bsevilla.v_dim_cp cp2 on u.zip_code = cp2.cp
			where u.leading_system in ('WEB','EVI')
			and u.last_interaction > '2016-08-28'
			and u.suffix <> 'entradas.com'
			and oe.ords = 1
			and cp2.provincia = cp.provincia 
			group by 1,2
			order by 3 desc) a
left join (
			select cp2.provincia provincia, count(u.customer_number) tot
			from user_bsevilla.v_users u 
			left join (
						select customer_number, count(order_number) as ords
						from user_bsevilla.v_orders
						group by 1
					  ) oe on oe.customer_number = u.customer_number
			left join user_bsevilla.v_orders o on o.customer_number = u.customer_number
			left join zone_country_es.v_c_tcdb__dbo__event e on e.id = o.evid
			left join zone_country_es.v_c_tcdb__dbo__venue v on e.ve_id = v.id 
			left join user_bsevilla.v_dim_cp cp on v.plz = cp.cp
			left join user_bsevilla.v_dim_cp cp2 on u.zip_code = cp2.cp
			where u.leading_system in ('WEB','EVI')
			and u.last_interaction > '2016-08-28'
			and u.suffix <> 'entradas.com'
			and oe.ords = 1
			and cp2.provincia = cp.provincia 
			group by 1) b on a.provincia = b.provincia
order by 4 desc
;
-- raul calle abubilla 952291367;

select * from zone_country_es.v_c_tcdb__dbo__kunde order by rand() limit 100 ;

