-- mailing KPIs 2019

select 
	case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar'
	end as mailing_type,
	to_char(send_date, 'YYYYMM') as send_month,
	avg(u_opens)/avg(opt_net) as "OR",
	avg(u_clicks)/avg(opt_net) as ctr,
	avg(unsubscriptions)/avg(opt_net) as unsubs,
	sum(crm_tix) as crm_tix,
	sum(crm_tix_direct) as crm_tix_direct,
	sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-01'
and send_date < '2019-01-01'
and (lower(mailing_type_name) like '%insider%'
or lower(mailing_type_name) like '%regio%'
or lower(mailing_type_name) like '%special%'
or lower(mailing_type_name) like '%cinema%'
or lower(mailing_type_name) like '%similar%')
group by 1,2
order by 1,2
;


select distinct mailing_type_name from zone_country_es.v_c_dama_csr
;

select * from zone_country_es.v_c_dama_csr 
where mailing_name = 'Regio_campaign_RECO_AVAILABILITY_INFO>10_20190723 TGP_1_T1RE_Regio_9RE'
limit 10;

-- insiders

select 
	--to_char(send_date, 'YYYYMM') as send_month,
	camp_name,
	min(send_date),
	sum(opt_net) as recipients,
	sum(u_opens) as u_opens
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2019-05-01'
and send_date < '2019-07-31'
and (lower(mailing_type_name) like '%regio%')
group by 1
order by 2
--order by 1,2
;

select * from zone_country_es.v_c_dama_csr 
where (lower(mailing_type_name) like '%insider%')
and send_date > '2019-06-01'
and send_date < '2019-07-31'
limit 10;

-- OR insider y regio de los �ltimos 4 meses por tipo de email

select * from zone_country_es.v_c_optf__comb__mailingrecipients limit 10;

select to_char(send_date, 'YYYYMM') as send_month,
		csr.mailing_type_name,
		count(mo.pii_userid)/count(mr.pii_userid)
from zone_country_es.v_c_optf__comb__mailingrecipients mr
left join zone_country_es.v_c_optf__comb__opens mo on (mr.pii_userid = mo.pii_userid and mr.mailingid = mo.mailingid)
left join zone_country_es.v_c_dama_csr csr on csr.mailing_id = mr.mailingid
--left join zone_country_es.v_c_dama_ci ci on ci.customer_email = mr.pii_userid
where mo.mailingid is not null
and mr.sendingdate > '2019-05-01'
and mr.sendingdate < '2019-09-01'
and (lower(csr.mailing_type_name) like '%insider%'
	or lower(csr.mailing_type_name) like '%regio%')
group by 1,2
order by 2,1
;

select a.send_month,
	   --a.send_year,
	   a.mailing_type,
	   a.suffix,
	   sum(a.opens)/sum(a.recipients) as open_rate,
	   sum(a.recipients) as recipients
from 	(
		select to_char(csr.send_date, 'YYYYMM') as send_month,
				to_char(csr.send_date, 'YYYY') as send_year,
				csr.mailing_type_name as mailing_type, 
				csr.mailing_name as mailing_name,
				ci.customer_email_suffix as suffix,
				count(distinct mr.pii_userid) as recipients,
				count(distinct mo.pii_userid) as opens
		from zone_country_es.v_c_dama_csr csr
		left join zone_country_es.v_c_optf__comb__mailingrecipients mr on csr.mailing_id = mr.mailingid
		left join zone_country_es.v_c_optf__comb__opens mo on (mr.pii_userid = mo.pii_userid and mr.mailingid = mo.mailingid) 
		left join zone_country_es.v_c_dama_ci ci on ci.customer_email = mr.pii_userid
		where 1=1
		and csr.send_date > '2018-01-01'
		--and csr.send_date < '2019-09-01'
		and (lower(csr.mailing_type_name) like '%insider%'
			or lower(csr.mailing_type_name) like '%regio%')
		group by 1,2,3,4,5
		) a
where a.suffix in ('gmail.com','hotmail.com','yahoo.es','hotmail.es','PRIVATE')
group by 1,2,3
order by 2,1 desc
;

select mr.mailingid, sum(case when mr.pii_userid is null then 0 else 1 end) as recipients
from zone_country_es.v_c_optf__comb__mailingrecipients mr
where mr.mailingid = 261207469093
group by 1;


-- are or and opt_net correlated?

select opt_net, u_opens/opt_net, mailing_type_name
from zone_country_es.v_c_dama_csr
where send_date > '2018-09-10'
and lower(mailing_name) not like '%resend%'
and (lower(mailing_type_name) like '%insider%'
or lower(mailing_type_name) like '%regio%'
or lower(mailing_type_name) like '%special%'
or lower(mailing_type_name) like '%similar%')
and opt_net > 500
;


-- email el medico
select  max(opt_net) as opt_net, sum(u_opens)/max(opt_net) as open_rate, sum(u_clicks)/max(opt_net) as ctr, 
		sum(crm_tix_direct) crm_tix_direct, sum(crm_tix) crm_tix, sum(combined_tix) combined_tix,
		sum(crm_tix_direct)/max(opt_net) as direct_rate, sum(crm_tix)/max(opt_net) crm_rate, sum(combined_tix)/max(opt_net) combined_rate
from zone_country_es.v_c_dama_csr csr 
where csr.mailing_name like 'COM_MUSL_20190909_ELMEDICO%'
;

-- birthday

select send_date, mailing_name, opt_net
from zone_country_es.v_c_dama_csr
where 1=1
--and mailing_name like '%messi%'
and send_date >= '2019-09-23'
and send_date <= '2019-09-29'
order by 1
;


select 
	case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar artist'
	end as mailing_type,
	to_char(send_date, 'YYYYMM') as send_month,
	--avg(u_opens)/avg(opt_net) as "OR",
	--avg(u_clicks)/avg(opt_net) as ctr,
	--avg(unsubscriptions)/avg(opt_net) as unsubs,
	sum(crm_tix) as crm_tix,
	count(distinct mailing_id) as mailings_sent,
	sum(crm_tix_direct) as crm_tix_direct,
	sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-01'
--and send_date < '2019-01-01'
and (lower(mailing_type_name) like '%similar%')
group by 1,2
order by 1,2
;


select  to_char(send_date, 'YYYYMMDD') as send_date,
	    sum(opt_net) as recipients,
	    sum(u_opens) as u_opens,
	    sum(u_clicks) as u_clicks,
	    sum(unsubscriptions) as unsubs,
		sum(crm_tix) as crm_tix,
		sum(crm_tix_direct) as crm_tix_direct,
		sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2019-01-01'
--and send_date < '2019-01-01'
and (lower(mailing_type_name) like '%abandonment%')
group by 1
order by 1
;


select  to_char(send_date, 'YYYYMMDD') as send_date,
		mailing_name,
	    sum(opt_net) as recipients,
	    sum(u_opens) as u_opens,
	    sum(u_clicks) as u_clicks,
	    sum(unsubscriptions) as unsubs,
		sum(crm_tix) as crm_tix,
		sum(crm_tix_direct) as crm_tix_direct,
		sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2019-09-01'
and send_date < '2019-10-01'
and (lower(mailing_type_name) like '%insider%' or lower(mailing_name) like '%insider%')
group by 1,2
order by 1,2
;		

select to_char(send_date, 'YYYY-MM-DD') as send_date, mailing_name, opt_net, round(100*u_opens/opt_net,2) 'OR', combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-01'
and lower(mailing_name) like '%cantajuego%'
order by 1
;

select  to_char(send_date, 'YYYYMMDD') as send_date,
		mailing_name,
	    sum(opt_net) as recipients,
	    sum(u_opens) as u_opens,
	    sum(u_clicks) as u_clicks,
	    sum(unsubscriptions) as unsubs,
		sum(crm_tix) as crm_tix,
		sum(crm_tix_direct) as crm_tix_direct,
		sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2019-01-01'
--and send_date < '2019-11-01'
and (lower(mailing_name) like '%duncan%')
group by 1,2
order by 1,2
;	



select * 
select count(distinct customer_number) from zone_country_es.v_c_dama_ci where customer_federal_region in ('Sevilla', 'M�laga', 'Huelva', 'C�diz') and last_order_date > '2016-11-13';