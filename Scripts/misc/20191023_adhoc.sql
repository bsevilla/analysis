select
	 count(customer_federal_region) as federal_region
	,count(customer_zip_code) as zip_code
	,count(customer_city_name) as city_name
	,count(case when customer_federal_region is null then customer_city_name end) city_no_federal
	,count(case when customer_city_name is null then customer_federal_region end) federal_no_city
from zone_country_es.v_c_dama_ci
where customer_country_iso2 = 'ES';

select
	case when customer_zip_code is null and customer_city_name is null and customer_federal_region is null then '000'
		 when customer_zip_code is null and customer_city_name is null and customer_federal_region is not null then '001'
		 when customer_zip_code is null and customer_city_name is not null and customer_federal_region is null then '010'
		 when customer_zip_code is null and customer_city_name is not null and customer_federal_region is not null then '011'
		 when customer_zip_code is not null and customer_city_name is null and customer_federal_region is null then '100'
		 when customer_zip_code is not null and customer_city_name is null and customer_federal_region is not null then '101'
		 when customer_zip_code is not null and customer_city_name is not null and customer_federal_region is null then '110'
		 when customer_zip_code is not null and customer_city_name is not null and customer_federal_region is not null then '111'
		 else 'error' end as status, count(customer_number)
from zone_country_es.v_c_dama_ci
where customer_country_iso2 = 'ES'
and order_cnt_all > 0
group by 1;

select
	 customer_city_name
--	,customer_zip_code
	,count(*)
from ZONE_COUNTRY_ES.V_C_DAMA_CI
where customer_country_iso2 = 'ES'
--and customer_city_name = 'Valencia'
and customer_Zip_code = '46022'
group by 1
order by 2 desc
;

select u.customer_federal_region,
	   count(distinct u.customer_number)
from zone_country_es.v_c_dama_ci u
left join zone_country_es.v_c_tcdb__dbo__order_header oh on u.customer_number = oh.kundennummer
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.ordernummer = op.ordernummer
left join zone_country_es.v_dc_lnk__artist__eventseries ea on op.er_id = ea.eventseries_id
where ea.artist_id = 565075
group by 1
order by 2 desc
;

select distinct federal_region from user_bsevilla.v_users;

select ci.leading_system, nc.newsletter_category_name, count(distinct ci.customer_number)
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_dc_lnk__customer__newsletter cn on cn.customer_number = ci.customer_number
left join zone_country_es.v_dc_newsletter_category nc on cn.newsletter_category_id = nc.newsletter_category_id
where ci.order_cnt_all = 0
group by 1,2
order by 1 desc,2
;
	
select distinct cn.newsletter_category_id, nc.newsletter_category_name from zone_country_es.v_dc_lnk__customer__newsletter cn
left join zone_country_es.v_dc_newsletter_category nc on cn.newsletter_category_id = nc.newsletter_category_id
limit 10;

select count(distinct customer_number)
from zone_country_es.v_dc_dama_ci
where order_rec_all between 335 and 365
;

select * from zone_country_es.v_dc_genre where genre_id > 0 and genre_id < 500 order by 3;

