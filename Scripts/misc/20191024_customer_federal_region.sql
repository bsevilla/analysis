select distinct
--	 country_code
	case when length(zd.zip) = 5 then zd.zip else concat('0',zd.zip) end as zip
	,n3.name as nuts3_name
	,region2_name
	,cp.provincia
	--,zd.federal_region
	--,provincia
	,count(distinct customer_number)
from zone_country_es.v_c_man__zip_data zd
left join zone_country_es.v_c_dim_geo_postcode pc on regexp_replace(pc.postcode,'[^\w]','') = case when length(zd.zip) = 5 then zd.zip else concat('0',zd.zip) end
left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
left join user_bsevilla.v_dim_cp cp on cp.cp = case when length(zip) = 5 then zip else concat('0',zip) end
left join zone_country_es.v_c_dama_ci ci on ci.customer_zip_code = case when length(zd.zip) = 5 then zd.zip else concat('0',zd.zip) end
where zd.country_code = 'ES'
and zd.federal_region is null
group by 1,2,3,4
order by 2
;

select distinct
--		regexp_replace(pc.postcode,'[^\w]','') as zip_code
--		,n2.name as nuts2_name
--		,region1_name
		n3.name as nuts3_name
		,region2_name
		,provincia
from zone_country_es.v_c_dim_geo_postcode pc
left join zone_country_es.v_c_geopc__nuts n1 on (substr(pc.nuts,1,3) = n1.code and n1.level_res_kw = 1)
left join zone_country_es.v_c_geopc__nuts n2 on (substr(pc.nuts,1,4) = n2.code and n2.level_res_kw = 2 )
left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
left join user_bsevilla.v_dim_cp cp on cp.cp = regexp_replace(pc.postcode,'[^\w]','')
where pc.iso = 'ES'
;

select distinct
		regexp_replace(pc.postcode,'[^\w]','') as zip_code
--		,n2.name as nuts2_name
--		,region1_name
		,n3.name as nuts3_name
		,region2_name
		,provincia
from zone_country_es.v_c_dim_geo_postcode pc
left join zone_country_es.v_c_geopc__nuts n1 on (substr(pc.nuts,1,3) = n1.code and n1.level_res_kw = 1)
left join zone_country_es.v_c_geopc__nuts n2 on (substr(pc.nuts,1,4) = n2.code and n2.level_res_kw = 2 )
left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
left join user_bsevilla.v_dim_cp cp on cp.cp = regexp_replace(pc.postcode,'[^\w]','')
where pc.iso = 'ES'
;


--create or replace view user_bsevilla.v_zip_code_lookup as
select distinct
		regexp_replace(pc.postcode,'[^\w]','') as zip_code
		,case when region2_name is not null then region2_name
		else 
			case when cp.provincia is not null then cp.provincia
			else cp2.provincia end 
		end as federal_region
from zone_country_es.v_c_dim_geo_postcode pc
left join zone_country_es.v_c_geopc__nuts n1 on (substr(pc.nuts,1,3) = n1.code and n1.level_res_kw = 1)
left join zone_country_es.v_c_geopc__nuts n2 on (substr(pc.nuts,1,4) = n2.code and n2.level_res_kw = 2 )
left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
left join user_bsevilla.v_dim_cp cp on cp.cp = regexp_replace(pc.postcode,'[^\w]','')
left join user_bsevilla.v_dim_cp cp2 on cp2.code = substr(regexp_replace(pc.postcode,'[^\w]',''),1,2)
where pc.iso = 'ES'
union
select distinct cp, provincia
from user_bsevilla.v_dim_cp 
where length(cp) = 4
;



select regexp_replace(pc.postcode,'[^\w]','') from zone_country_es.v_c_dim_geo_postcode pc;

with new_lookup as 
(
select distinct
		regexp_replace(pc.postcode,'[^\w]','') as zip_code
		,case when region2_name is not null then region2_name
		else 
			case when cp.provincia is not null then cp.provincia
			else cp2.provincia end 
		end as federal_region
from zone_country_es.v_c_dim_geo_postcode pc
left join zone_country_es.v_c_geopc__nuts n1 on (substr(pc.nuts,1,3) = n1.code and n1.level_res_kw = 1)
left join zone_country_es.v_c_geopc__nuts n2 on (substr(pc.nuts,1,4) = n2.code and n2.level_res_kw = 2 )
left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
left join user_bsevilla.v_dim_cp cp on cp.cp = regexp_replace(pc.postcode,'[^\w]','')
left join user_bsevilla.v_dim_cp cp2 on cp2.code = substr(regexp_replace(pc.postcode,'[^\w]',''),1,2)
where pc.iso = 'ES'
union
select distinct cp, provincia
from user_bsevilla.v_dim_cp 
where length(cp) = 4
)
select
	 case when c.customer_federal_region = nl.federal_region then 'same'
		  when nl.federal_region is null and c.customer_federal_region is not null then 'new is null'
		  when nl.federal_region is not null and c.customer_federal_region is null then 'old is null'
		  when nl.federal_region is null and c.customer_federal_region is null then 'both are null'
		  else 'changed' end as bucket
	,count(*) as c
	,count(Distinct c.customer_federal_region) as old_regions_count
	,count(Distinct nl.federal_region) as new_regions_count
from Zone_country_es.v_c_dama_ci c
left join new_lookup nl on nl.zip_code = c.customer_zip_code
where c.customer_country_iso2 = 'ES'
group by 1
order by 2 desc
;




with new_lookup as 
(
	select distinct
		regexp_replace(pc.postcode,'[^\w]','') as zip_code
		,case when region2_name is not null then region2_name
			else
				case when cp.provincia is not null then cp.provincia
				else cp2.provincia end
			end as federal_region
	from zone_country_es.v_c_dim_geo_postcode pc
	left join zone_country_es.v_c_geopc__nuts n1 on (substr(pc.nuts,1,3) = n1.code and n1.level_res_kw = 1)
	left join zone_country_es.v_c_geopc__nuts n2 on (substr(pc.nuts,1,4) = n2.code and n2.level_res_kw = 2 )
	left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
	left join user_bsevilla.v_dim_cp cp on cp.cp = regexp_replace(pc.postcode,'[^\w]','')
	left join user_bsevilla.v_dim_cp cp2 on cp2.code = substr(regexp_replace(pc.postcode,'[^\w]',''),1,2)
	where pc.iso = 'ES'
)
select
	 case when c.customer_federal_region = nl.federal_region then 'same'
		  when nl.federal_region is null and c.customer_federal_region is not null then 'new is null'
		  when nl.federal_region is not null and c.customer_federal_region is null then 'old is null'
		  when nl.federal_region is null and c.customer_federal_region is null then 'both are null'
		  else 'changed' end as bucket,
	c.customer_federal_region, nl.federal_region, c.customer_city_name, c.customer_zip_code	  
	--,count(*) as c
	--,count(Distinct c.customer_federal_region) as old_regions_count
	--,count(Distinct nl.federal_region) as new_regions_count
from Zone_country_es.v_c_dama_ci c
left join new_lookup nl on nl.zip_code = c.customer_zip_code
where c.customer_country_iso2 = 'ES'
and c.leading_system in ('WEB', 'EVI', 'KNH')
and nl.federal_region is null and c.customer_federal_region is null
--group by 1
--order by 2 desc
;

---------------------------



select
	 COALESCE(trim(to_char(cast(cp as dec),'00000')),trim(to_char(cast(ZIP as dec),'00000'))) as zip_final
	,ZIP as zip_old -- missing leading 0s
	,trim(to_char(cast(ZIP as dec),'00000')) as zip
	,trim(to_char(cast(cp as dec),'00000')) as cp
	,z.CITY_NAME
	,cp.PUEBLO
	,COALESCE(cp.PUEBLO,z.CITY_NAME) as city_name_combined
	,z.PROVINCE
	,cp.PROVINCIA
	,COALESCE(cp.PROVINCIA,z.PROVINCE) as PROVINCE_combined
from ZONE_COUNTRY_ES.V_C_MAN__ZIP_DATA z
full outer join user_bsevilla.v_dim_cp cp on trim(to_char(cast(cp as dec),'00000')) = trim(to_char(cast(ZIP as dec),'00000'))
where COALESCE(z.country_code,'ES') = 'ES'
union
select
	 case when length(zip) = 4 then zip else cp end as zip_final
	,ZIP as zip_old -- missing leading 0s
	,trim(to_char(cast(ZIP as dec),'00000')) as zip
	,trim(to_char(cast(cp as dec),'00000')) as cp
	,z.CITY_NAME
	,cp.PUEBLO
	,COALESCE(cp.PUEBLO,z.CITY_NAME) as city_name_combined
	,z.PROVINCE
	,cp.PROVINCIA
	,COALESCE(cp.PROVINCIA,z.PROVINCE) as PROVINCE_combined
from ZONE_COUNTRY_ES.V_C_MAN__ZIP_DATA z
full outer join user_bsevilla.v_dim_cp cp on trim(to_char(cast(cp as dec),'00000')) = trim(to_char(cast(ZIP as dec),'00000'))
where COALESCE(z.country_code,'ES') = 'ES'
and (length(zip) = 4 or length(cp) = 4)
;


--- ciudades fuzzy match

--select customer_city_name, first_value(pueblo), first_value(provincia)
--from (
select distinct case when ci.customer_city_name is null and cp.poblacion is null then 'both null'
			when ci.customer_city_name is null and cp.poblacion is not null then 'city is null'
			when ci.customer_city_name is not null and cp.poblacion is null then 'pobl is null'
			else 'neither is null'
			end as a, 
			trim(ci.customer_city_name, ' ') as customer_city_name, 
			ci.customer_federal_region, 
			cp.pueblo, 
			cp.provincia, 
			cp.poblacion,
			abs(len(trim(ci.customer_city_name,' '))-len(cp.pueblo)) diff
from zone_country_es.v_c_dama_ci ci
left join user_bsevilla.v_dim_cp cp on concat('%', lower(ci.customer_city_name), '%') like concat('%', lower(cp.pueblo), '%')
where ci.customer_city_name is not null and cp.poblacion is not null
order by 2,7,6
--) group by 1
;

select distinct customer_city_name, provincia, poblacion
from (
		select customer_city_name, pueblo, provincia, poblacion, diff, users,
		rank() over (partition by customer_city_name order by diff, users desc) rnk
		from (
				select distinct case when ci.customer_city_name is null and cp.poblacion is null then 'both null'
					when ci.customer_city_name is null and cp.poblacion is not null then 'city is null'
					when ci.customer_city_name is not null and cp.poblacion is null then 'pobl is null'
					else 'neither is null'
					end as a, 
					trim(ci.customer_city_name, ' ') as customer_city_name, 
					ci.customer_federal_region, 
					cp.pueblo, 
					cp.provincia, 
					cp.poblacion,
					abs(len(trim(ci.customer_city_name,' '))-len(cp.pueblo)) diff,
					count(distinct ci.customer_number) users
				from zone_country_es.v_c_dama_ci ci
				left join user_bsevilla.v_dim_cp cp on concat('%', lower(ci.customer_city_name), '%') like concat('%', lower(cp.pueblo), '%')
				where ci.customer_city_name is not null and cp.poblacion is not null
				group by 1,2,3,4,5,6,7
				order by 2,7,6
		)
)
where rnk = 1
;

case when c.customer_federal_region = nl.federal_region then 'same'
		  when nl.federal_region is null and c.customer_federal_region is not null then 'new is null'
		  when nl.federal_region is not null and c.customer_federal_region is null then 'old is null'
		  when nl.federal_region is null and c.customer_federal_region is null then 'both are null'
		  else 'changed' end as bucket
	,count(*) as c


select case when customer_federal_region = federal_region then 'same'
			when federal_region is null and customer_federal_region is not null then 'new is null'
		  when federal_region is not null and customer_federal_region is null then 'old is null'
		  when federal_region is null and customer_federal_region is null then 'both are null'
		  else 'changed' end as bucket,
	   count(*) as cnt
from 
(
	with new_lookup as 
	(
	select distinct
			regexp_replace(pc.postcode,'[^\w]','') as zip_code
			,case when region2_name is not null then region2_name
			else 
				case when cp.provincia is not null then cp.provincia
				else cp2.provincia end 
			end as federal_region
	from zone_country_es.v_c_dim_geo_postcode pc
	left join zone_country_es.v_c_geopc__nuts n1 on (substr(pc.nuts,1,3) = n1.code and n1.level_res_kw = 1)
	left join zone_country_es.v_c_geopc__nuts n2 on (substr(pc.nuts,1,4) = n2.code and n2.level_res_kw = 2 )
	left join zone_country_es.v_c_geopc__nuts n3 on (pc.nuts = n3.code and n3.level_res_kw = 3 )
	left join user_bsevilla.v_dim_cp cp on cp.cp = regexp_replace(pc.postcode,'[^\w]','')
	left join user_bsevilla.v_dim_cp cp2 on cp2.code = substr(regexp_replace(pc.postcode,'[^\w]',''),1,2)
	where pc.iso = 'ES'
	union
	select distinct cp, provincia
	from user_bsevilla.v_dim_cp 
	where length(cp) = 4
	),
	new_lookup2 as
	(select distinct customer_city_name, provincia as federal_region
	from (
			select customer_city_name, pueblo, provincia, poblacion, diff, users,
			rank() over (partition by customer_city_name order by diff, users desc) rnk
			from (
					select distinct case when ci.customer_city_name is null and cp.poblacion is null then 'both null'
						when ci.customer_city_name is null and cp.poblacion is not null then 'city is null'
						when ci.customer_city_name is not null and cp.poblacion is null then 'pobl is null'
						else 'neither is null'
						end as a, 
						trim(ci.customer_city_name, ' ') as customer_city_name, 
						ci.customer_federal_region, 
						cp.pueblo, 
						cp.provincia, 
						cp.poblacion,
						abs(len(trim(ci.customer_city_name,' '))-len(cp.pueblo)) diff,
						count(distinct ci.customer_number) users
					from zone_country_es.v_c_dama_ci ci
					left join user_bsevilla.v_dim_cp cp on concat('%', lower(ci.customer_city_name), '%') like concat('%', lower(cp.pueblo), '%')
					where ci.customer_city_name is not null and cp.poblacion is not null
					group by 1,2,3,4,5,6,7
					order by 2,7,6
			)
	)
	where rnk = 1)
	select c.customer_federal_region, c.customer_city_name, c.customer_zip_code,
		 case when nl.federal_region is not null then nl.federal_region
		 when nl2.federal_region is not null then nl2.federal_region
		 else null end as federal_region
	from zone_country_es.v_c_dama_ci c
	left join new_lookup nl on nl.zip_code = c.customer_zip_code
	left join new_lookup2 nl2 on c.customer_city_name = nl2.customer_city_name
	where c.customer_country_iso2 = 'ES'
)
group by 1
order by 2 desc
;


select customer_federal_region, count(distinct customer_number) from zone_country_es.v_c_dama_ci where leading_system in ('WEB', 'EVI') group by 1 order by 2 desc;
;

