

select  to_char(send_date, 'YYYYMMDD') as send_date,
		mailing_name,
	    opt_net as recipients,
	    u_opens as u_opens,
	    u_clicks as u_clicks,
	    unsubscriptions as unsubs,
		crm_tix as crm_tix,
		crm_tix_direct as crm_tix_direct,
		combined_tix as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-01'
--and send_date < '2019-11-01'
and lower(mailing_type_name) like '%insider%' 
and lower(mailing_name) not like '%resend%'
and lower(mailing_name) not like '%reopen%'
and lower(mailing_name) not like '%noopen%'
and opt_net > 5
order by 1,2
;	

with r as (
select  to_char(csr.send_date, 'YYYYMMDD') as send_date,
		csr.mailing_name,
		case when ol.order_num > 2 then 3 else ol.order_num end as order_cnt,
		u.entry_product,
		case when mr.pii_userid is null then 0 else 1 end as recipients,
		case when mo.pii_userid is null then 0 else 1 end as opens,
		case when mc.pii_userid is null then 0 else 1 end as clicks,
		case when mt.pii_userid is null then 0 else 1 end as sales
from zone_country_es.v_c_dama_csr csr
left join zone_country_es.v_c_optf__comb__mailingrecipients mr on mr.mailingid = csr.mailing_id
left join zone_country_es.v_c_optf__comb__opens mo on mo.mailingid = mr.mailingid and mo.pii_userid = mr.pii_userid
left join zone_country_es.v_c_optf__comb__clicks mc on mo.mailingid = mc.mailingid and mo.pii_userid = mc.pii_userid
left join (
			select distinct csr.mailing_id,
							om.order_id,
							ci.customer_email pii_userid
			from zone_country_es.v_fact_order_mailing om
			left join zone_country_es.v_c_dama_csr csr on csr.il_mailing_id = om.il_mailing_id_referrer
			left join zone_country_es.v_fact_order_base ob on ob.order_id = om.order_id
			left join zone_country_es.v_c_dama_ci ci on ci.customer_number = ob.customer_number
			where mailing_id is not null
			) mt on mt.mailing_id = mr.mailingid and mr.pii_userid = mt.pii_userid
left join user_bsevilla.v_orderdate_lookup ol on ol.customer_email = mr.pii_userid and to_char(csr.send_date, 'YYYYMMDD') = ol.order_date
left join user_bsevilla.v_users u on u.pii_customer_email = mr.pii_userid
where 1=1
and u.honesty_status > 0
and csr.send_date > '2018-01-01'
and csr.send_date < '2019-11-01'
and lower(csr.mailing_type_name) like '%insider%' 
and lower(csr.mailing_name) not like '%resend%'
and lower(csr.mailing_name) not like '%reopen%'
and lower(csr.mailing_name) not like '%noopen%'
and csr.opt_net > 5
order by 1,2
)
select order_cnt, entry_product, sum(recipients), sum(opens), sum(clicks), sum(sales), count(*)
from r
group by 1,2
;

-------------------------------------- compras por tiempo ----------------------------------------

/*
create or replace view user_bsevilla.v_orderdate_lookup as
(
with base as (
select distinct ci.customer_email, d.order_date, coalesce(o.order_num,0) order_num
from zone_country_es.v_c_dama_ci ci
cross join (select distinct to_char(orderdatum, 'YYYYMMDD') order_date
			from zone_country_es.v_c_tcdb__dbo__order_header
			where orderdatum > '2018-01-01'
			and orderdatum < '2019-11-01'
			order by 1) d
left join ( select customer_number, to_char(order_date, 'YYYYMMDD') order_date, count (distinct order_number) order_num
			from zone_country_es.v_fact_order_base
			where order_status = 100
			group by 1,2
			) o on o.customer_number = ci.customer_number and o.order_date = d.order_date
)
select base.customer_email, base.order_date,
	   sum(base.order_num) over(partition BY base.customer_email ORDER BY base.order_date) order_num
from base
)			
; */

select distinct vertical, event_name
from user_bsevilla.v_orders
where order_date > '2019-09-01'
and vertical = 'Sin categoria'
limit 10;

select * from zone_country_es.v_c_dama_ci limit 10;

create table user_bsevilla.dummy_table (
									code varchar(10),
									provincia varchar(30),
									pueblo varchar(70),
									cp varchar(10),
									latitud decimal(9,6),
									longitud decimal(9,6),
									poblacion decimal,
									geo geometry(4326)
									
);

drop table user_bsevilla.dummy_table;
