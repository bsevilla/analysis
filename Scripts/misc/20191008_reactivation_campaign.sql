-- �qu� probabilidad tiene una persona de volver a comprar cuando se va a quedar inactiva? --> 
-- �hay gente que haya estado 2.5 a�os sin tocar nada y se haya reactivado?

select distinct u.customer_number, cp.provincia, ob.order_date, ev.event_name
from user_bsevilla.v_users u
left join zone_country_es.v_fact_order_base ob on ob.customer_number = u.customer_number
left join zone_country_es.v_c_dama_event ev on ob.event_id = ev.event_id
left join user_bsevilla.v_dim_cp cp on cp.cp = u.zip_code
where u.honesty_status > 0
and u.leading_system in ('WEB', 'EVI')
and u.last_interaction > '2016-08-10'
and ob.order_date > '2013-08-10'
and coalesce(ob.order_status,100) in (100,300)
and coalesce(ob.order_delivery_status,2) >= 0
and coalesce(ob.order_delivery_status,2) != 9
and coalesce(ob.product_type_id,1) = 1 
and coalesce(ob.artist_rank,1) = 1
and coalesce(ob.genre_rank,1) = 1
and coalesce(ob.voucher_rank,1) = 1
and ob.ticket_rank = 1 
and ob.position_rank = 1
order by 1,2
;

select * from user_bsevilla.v_orders where customer_number in (950450416,952159217,950005356,950045561,951902640,951864545) order by order_date;

select ob.order_number, ob.order_date,  ob.event_id, ob.source_name, ob.order_value, ob.basket_size, ev.event_name
from zone_country_es.v_fact_order_base ob
left join zone_country_es.v_c_dama_event ev on ob.event_id = ev.event_id
where 1=1
and coalesce(order_status,100) in (100,300)
and coalesce(order_delivery_status,2) >= 0
and coalesce(order_delivery_status,2) != 9
and coalesce(product_type_id,1) = 1 
and coalesce(artist_rank,1) = 1
and coalesce(genre_rank,1) = 1
and coalesce(voucher_rank,1) = 1
and ticket_rank = 1 
and position_rank = 1
and customer_number = 951713959
order by 2
;

select source_name, order_value
from zone_country_es.v_fact_order_base 
where customer_number = 951188897
group by 1;

select kundenemail from zone_country_es.v_c_tcdb__dbo__kunde where kundennummer = 951902640;

select * from zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link where ku_id = 543633;
select * from zone_country_es.v_c_tcdb__dbo__eventreihe where id in (2004190,2121125);

select * from zone_country_es.v_c_tcdb__dbo__kuenstler where name = 'Mundo obrero'

select ob.*
from zone_country_es.v_fact_order_base ob -- aqu� est�n todas las orders de web enf knh y enl
left join zone_country_es.v_c_tcdb__dbo__kunde k on k.kundennummer = ob.customer_number -- aqu� la info de los usuarios de web (por eso si quisieras otro leading system tendr�a que ser otra tabla)
where 1=1 -- todo esto es incomprensible para m�, pero me lo pasaron as� y creo que son condiciones para que la order sea v�lida
and coalesce(ob.order_status,100) in (100,300)
and coalesce(ob.order_delivery_status,2) >= 0
and coalesce(ob.order_delivery_status,2) != 9
and coalesce(ob.product_type_id,1) = 1
and coalesce(ob.artist_rank,1) = 1
and coalesce(ob.genre_rank,1) = 1
and coalesce(ob.voucher_rank,1) = 1
and ob.ticket_rank = 1
and ob.position_rank = 1
-- and ob.source_name in ('WEB', 'EVI') -- (descomenta esta linea si quieres solo orders de web)
and k.kundenemail = 'helpdesk@entradas.com' -- aqu� lo que quieras
limit 10;


select case when order_cnt = 1 then 'OTB'
when order_cnt < 1 then 'other' else '>1' end as user_type, count(distinct customer_number)
from user_bsevilla.v_users
where last_interaction > '2016-11-01'
and last_interaction < '2016-12-01'
--and leading_system = 'WEB'
group by 1
;