-- prueba reco 2
select count(customer_number)
from user_bsevilla.v_users
where leading_system in ('WEB', 'EVI')
and reco_level = 2
and entry_product = 'Culture'
;

select count(customer_number)
from user_bsevilla.v_users
where leading_system in ('WEB', 'EVI')
and last_interaction > '2016-08-26'
and reco_level = 2
and entry_product = 'Culture'
;

-- wtf is up con los reco1 y reco2

select to_char(send_date,'YYYY-MM-DD'), 
		case when csr.target_group_name like '%RRE1%' then 'reco1'
			 when csr.target_group_name like '%RRE2%' then 'reco2' end as reco_group,
		sum(csr.opt_net) as opt_net, sum(csr.u_opens) as u_opens, sum(csr.u_opens)/sum(csr.opt_net) as open_rate, sum(combined_tix)/sum(csr.opt_net) as cvr
from zone_country_es.v_c_dama_csr csr
--left join zone_country_es.v_c_optf__comb__mailingrecipients mr on mr.mailingid = csr.mailing_id
--left join zone_country_es.v_c_optf__comb__opens mo on mo.mailingid = mr.mailingid and mo.pii_userid = mr.pii_userid
where lower(csr.mailing_type_name) like '%insider%'
--and csr.send_date > '2017-09-23'
and csr.send_date > '2018-12-01'
and csr.send_date < '2018-12-12'
and (csr.target_group_name like '%RRE1%' or csr.target_group_name like '%RRE2%')
and csr.opt_net > 10
and lower(csr.mailing_name) not like '%test%'
and lower(csr.mailing_name) not like '%edit%'
and csr.mailing_name not like '%_teatro%'
group by 1,2
order by 1,2
;

select case when csr.target_group_name like '%RRE1%' then 'reco1'
		    when csr.target_group_name like '%RRE2%' then 'reco2' end as reco_group,
		ci.reco_level, ci.order_cnt_sht, ci.order_cnt_mid, ci.order_cnt_lng, ci.order_cnt_all, u.suffix, u.gender, u.age_range, 
ci.monetary_value_mid, ci.monetary_value_lng, ci.monetary_value_sht, ci.monetary_value_all, ci.ticket_price_avg_all, 
u.entry_product, ci.status_newsletter, ci.newsletter_open_time_avg, u.registration_date, u.last_interaction,
case when o.max_ords < 4 then 1 else 0 end as is_ok
from zone_country_es.v_c_dama_csr csr
left join zone_country_es.v_c_optf__comb__mailingrecipients mr on mr.mailingid = csr.mailing_id
--left join zone_country_es.v_c_optf__comb__opens mo on mo.mailingid = mr.mailingid and mo.pii_userid = mr.pii_userid
left join zone_country_es.v_c_dama_ci ci on mr.pii_userid = ci.customer_email
left join user_bsevilla.v_users u on ci.customer_number = u.customer_number
left join (
			select customer_number, max(ords) as max_ords
			from (
					select customer_number, evid, count(order_number) as ords
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o on o.customer_number = u.customer_number
where lower(csr.mailing_type_name) like '%insider%'
and csr.send_date > '2018-12-01'
and csr.send_date < '2018-12-12'
and (csr.target_group_name like '%RRE1%' or csr.target_group_name like '%RRE2%')
and csr.opt_net > 10
and lower(csr.mailing_name) not like '%test%'
and lower(csr.mailing_name) not like '%edit%'
and csr.mailing_name not like '%_teatro%'
and ci.leading_system in ('WEB', 'EVI')
order by 
limit 100
;


