-- orders
select 
	ci.customer_number as customer_number, 
	oh.ordernummer as order_number,
	oh.orderdatum as order_date,
	oh.number_of_tickets as ticket_number,
	oh.order_total as order_total,
	op.er_id as erid,
	op.ev_id as evid,
	ev.name1 as event_name, 
	hk.bezeichnung as vertical
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_c_tcdb__dbo__order_header oh on oh.kundennummer = ci.customer_number 
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.id = op.oh_id
left join zone_country_es.v_c_tcdb__dbo__event ev on ev.id = op.ev_id
join zone_country_es.v_c_tdl__comb__vorstellung v on v.vorstellungid = ev.tdl_vorstellung_id
join zone_country_es.v_c_tdl__comb__veranstaltung ver on ver.veranstaltungid = v.veranstaltungid
join zone_country_es.v_c_tdl__comb__kategorie k on k.kategorieid = ver.kategorieid
join zone_country_es.v_c_tdl__comb__hauptkategorie hk on hk.hauptkategorieid = k.hauptkategorieid
;