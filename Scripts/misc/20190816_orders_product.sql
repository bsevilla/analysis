-- orders per user

select ci.entry_product, 
	   avg(oh.order_cnt) as 'count'
from user_bsevilla.v_users ci
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-08-01'
		group by 1
		) oh on oh.customer_number = ci.customer_number		
where ci.leading_system in ('WEB', 'EVI')
group by 1
;

