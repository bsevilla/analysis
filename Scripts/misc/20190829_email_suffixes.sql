select a.send_month,
	   --a.send_year,
	   a.mailing_type,
	   a.suffix,
	   sum(a.opens)/sum(a.recipients) as open_rate,
	   sum(a.recipients) as recipients
from 	(
		select to_char(csr.send_date, 'YYYYMM') as send_month,
				to_char(csr.send_date, 'YYYY') as send_year,
				csr.mailing_type_name as mailing_type, 
				csr.mailing_name as mailing_name,
				ci.customer_email_suffix as suffix,
				count(distinct mr.pii_userid) as recipients,
				count(distinct mo.pii_userid) as opens
		from zone_country_es.v_c_dama_csr csr
		left join zone_country_es.v_c_optf__comb__mailingrecipients mr on csr.mailing_id = mr.mailingid
		left join zone_country_es.v_c_optf__comb__opens mo on (mr.pii_userid = mo.pii_userid and mr.mailingid = mo.mailingid) 
		left join zone_country_es.v_c_dama_ci ci on ci.customer_email = mr.pii_userid
		where 1=1
		and lower(csr.mailing_name) not like '%resend%'
		and csr.send_date > '2018-01-01'
		and csr.send_date < '2019-09-01'
		and (lower(csr.mailing_type_name) like '%insider%'
			or lower(csr.mailing_type_name) like '%regio%')
		group by 1,2,3,4,5
		) a
where a.suffix in ('gmail.com','hotmail.com','yahoo.es','hotmail.es','PRIVATE')
group by 1,2,3
order by 2,1 desc
;

select to_char(csr.send_date, 'YYYYMM') as send_month,
				to_char(csr.send_date, 'YYYY') as send_year,
				csr.mailing_type_name as mailing_type, 
				csr.mailing_name as mailing_name,
				ci.customer_email_suffix as suffix,
				count(distinct mr.pii_userid) as recipients,
				count(distinct mo.pii_userid) as opens
		from zone_country_es.v_c_dama_csr csr
		left join zone_country_es.v_c_optf__comb__mailingrecipients mr on csr.mailing_id = mr.mailingid
		left join zone_country_es.v_c_optf__comb__opens mo on (mr.pii_userid = mo.pii_userid and mr.mailingid = mo.mailingid) 
		left join zone_country_es.v_c_dama_ci ci on ci.customer_email = mr.pii_userid
		where 1=1
		and csr.send_date > '2018-01-01'
		and csr.send_date < '2019-09-01'
		and (lower(csr.mailing_type_name) like '%insider%'
			or lower(csr.mailing_type_name) like '%regio%')
		group by 1,2,3,4,5;