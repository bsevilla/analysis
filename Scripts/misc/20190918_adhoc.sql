-- galicia

select customer_zip_code, count(customer_number)--, customer_federal_region, customer_city_name
from zone_country_es.v_c_dama_ci 
where customer_zip_code like '27%'
and (customer_country_code = 'ESP' or customer_country_code is null)
and (customer_federal_region = 'Galicia' or customer_federal_region is null)
group by 1
;

-- analysis variables
select customer_number, age_range, gender, registration_date, 
reco_level, order_cnt, last_order_date, last_interaction, 
first_event_name, entry_product, federal_region, customer_zip_code as zip_code
from user_bsevilla.v_users
where last_interaction > '2016-08-26'
and leading_system in ('WEB', 'EVI')
;

-- usuarios barcelona
select case when oh.order_date is not null then oh.order_date
			else 
				case when (ci.registration_date > '2017-07-01' and ci.registration_date < '2018-08-01') then
					'registered jul2017-18'
				when (ci.registration_date > '2018-08-01' and ci.registration_date < '2019-09-01') then
	   				'registered ago2018-19'
				else '1error' end 
			end as order_date,
	   count(ci.customer_number) as count
	   --ci.customer_number
from zone_country_es.v_c_dama_ci ci
left join ( 
				select distinct kundennummer, 
					case when (orderdatum > '2017-07-01' and orderdatum < '2018-08-01') then
						'bought jul2017-18'
					when (orderdatum > '2018-08-01' and orderdatum < '2019-09-01') then
						'bought ago2018-19' else '1error' end as order_date
				from zone_country_es.v_c_tcdb__dbo__order_header oh
				where (orderdatum > '2017-07-01' 
				and orderdatum < '2018-07-31') or 
				(orderdatum > '2018-08-01' 
				and orderdatum < '2019-08-31')
			) oh on oh.kundennummer = ci.customer_number
where ci.leading_system in ('WEB', 'EVI')
and ci.customer_federal_region in ('Catalu�a', 'Catalunya')
and (oh.order_date is not null 
	or (ci.registration_date > '2017-07-01' and ci.registration_date < '2018-08-01')
	or (ci.registration_date > '2018-08-01' and ci.registration_date < '2019-09-01'))
group by 1
order by 1 desc
;

select distinct oh.order_date, coh.order_date,
	   count(distinct ci.customer_number) as count
	   --ci.customer_number
from zone_country_es.v_c_dama_ci ci
left join ( 
				select distinct oh.kundennummer as kundennummer, 
					case when (oh.orderdatum > '2017-07-01' and oh.orderdatum < '2018-08-01') then
						'compr� en cat - jul2017-18'
					when (oh.orderdatum > '2018-08-01' and oh.orderdatum < '2019-09-01') then
						'compr� en cat - ago2018-19' else '1error' end as order_date
				from zone_country_es.v_c_tcdb__dbo__order_header oh
				left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.id = op.oh_id
				left join zone_country_es.v_c_tcdb__dbo__venue ve on op.ve_id = ve.id
				where ((oh.orderdatum > '2017-07-01' 
				and oh.orderdatum < '2018-07-31') or 
				(oh.orderdatum > '2018-08-01' 
				and oh.orderdatum < '2019-08-31'))
				and (ve.plz like '08___' or ve.plz like '17___' or ve.plz like '25___' or ve.plz like '43___')
			) oh on oh.kundennummer = ci.customer_number
left join ( 
				select distinct oh.kundennummer, 
					case when (oh.orderdatum > '2017-07-01' and oh.orderdatum < '2018-08-01') then
						'es de cat jul2017-18'
					when (oh.orderdatum > '2018-08-01' and oh.orderdatum < '2019-09-01') then
						'es de cat ago2018-19' else '1error' end as order_date
				from zone_country_es.v_c_tcdb__dbo__order_header oh
				left join zone_country_es.v_c_dama_ci ci on oh.kundennummer = ci.customer_number
				where ((oh.orderdatum > '2017-07-01' 
				and oh.orderdatum < '2018-07-31') or 
				(oh.orderdatum > '2018-08-01' 
				and oh.orderdatum < '2019-08-31'))
				and ci.customer_federal_region in ('Catalu�a', 'Catalunya')
			) coh on coh.kundennummer = ci.customer_number
where ci.leading_system in ('WEB', 'EVI')
--and ci.customer_federal_region in ('Catalu�a', 'Catalunya')
group by 1,2
order by 3,1,2 desc
;



-- billy 

select ci.customer_number, ci.age_range, ci.gender, ci.registration_date, 
ci.reco_level, ci.order_cnt, ci.last_order_date, ci.last_interaction, 
ci.first_event_name, ci.entry_product, ci.federal_region, ci.zip_code
from user_bsevilla.v_users ci
inner join (
select distinct customer_number 
from user_bsevilla.v_orders 
where erid = 1769242) obe on obe.customer_number = ci.customer_number
;

select year(o.order_date) as 'year', 
	   --ci.federal_region,
count(distinct o.customer_number) as 'orders'
from user_bsevilla.v_orders o
left join user_bsevilla.v_users ci on o.customer_number = ci.customer_number
where year(order_date) > 2016
--and ci.federal_region in (null, 'Madrid', 'Andaluc�a', 'Levante', 'Catalu�a', 'Norte', 'Castilla-La Mancha', 'Catalu�a', 'Catalunya', 'Castilla y leon', 'Comunidad Valenciana', 'Galicia')
and o.erid = 1769242
group by 1--,2
order by 1--,3 desc
;

select count(distinct ci.customer_number)
from user_bsevilla.v_users ci
left join (
			select customer_number, count(distinct erid) as unique_orders
			from user_bsevilla.v_orders
			group by 1
) uo on uo.customer_number = ci.customer_number
where ci.last_interaction > '2016-08-27'
and ci.federal_region not in (null, 'Madrid')--, 'Andaluc�a', 'Levante', 'Catalu�a', 'Norte', 'Castilla-La Mancha', 'Catalu�a', 'Catalunya', 'Castilla y leon', 'Comunidad Valenciana', 'Galicia')
and ci.customer_number not in (select distinct customer_number
							   from user_bsevilla.v_orders 
							   where erid = 1769242)
and ci.customer_number in (select distinct customer_number
							   from user_bsevilla.v_orders 
							   where erid in (1264317, 1281079, 1665641, 2050236, 1262539, 1279216, 2464649, 2151440, 2040895, 2026342, 1402825, 1284617, 1354745, 1403639, 1494689, 1264120, 1264318, 1264316, 1517815, 1426168, 1701849, 2293124, 1956111, 1622511, 1870348, 1608146, 1955849, 1873947, 1543925, 1628390, 1632802, 1426418, 1549874, 1887625, 1517825, 1955473, 1536656, 1547230, 1633553, 1521770, 1539006, 1557800, 1491583, 1896651, 2302523, 1752472, 1673922, 1527787, 1897205, 1568946, 1682810, 1496251, 1673314, 1673652, 1778601, 1313298, 1537004, 1607104, 1686204, 1501003, 1495237, 1526128, 1397856, 1450476, 1941891, 1677638, 1504807, 1600504, 1662840, 2083330, 1584756, 1616536, 1922488, 1870586, 2259289, 2032887, 1281348, 2061476, 2018029, 2259290, 2123111, 1458202, 1456438, 1702531, 1896077, 1389140, 1680354, 1280899, 1810810, 1703533, 2144235, 1565629, 2130734, 1439758, 1752872 ))						   
and uo.unique_orders >= 2
--and ci.entry_product = 'Musical'
--group by 1--,2
--order by 1--,3 desc
;
--2 orders o m�s : 133307
--3 orders o m�s : 93577


select count(distinct ci.customer_number)
from user_bsevilla.v_users ci
where ci.last_interaction > '2016-08-27'
and ci.federal_region not in (null, 'Madrid')--, 'Andaluc�a', 'Levante', 'Catalu�a', 'Norte', 'Castilla-La Mancha', 'Catalu�a', 'Catalunya', 'Castilla y leon', 'Comunidad Valenciana', 'Galicia')
and ci.customer_number not in (select distinct customer_number
							   from user_bsevilla.v_orders 
							   where erid = 1769242)
and ci.customer_number in (select distinct customer_number
							   from user_bsevilla.v_orders 
							   where erid in (1264317, 1281079, 1665641, 2050236, 1262539, 1279216, 2464649, 2151440, 2040895, 2026342, 1402825, 1284617, 1354745, 1403639, 1494689, 1264120, 1264318, 1264316, 1517815, 1426168, 1701849, 2293124, 1956111)
							   and order_date > '2018-01-01')
and ci.order_cnt >= 2
and ci.age_range in ('undefined','55-90','45-54')
--and ci.entry_product = 'Musical'
--group by 1--,2
--order by 1--,3 desc
;

-- tuppersex


select id, name from zone_country_es.v_c_tcdb__dbo__eventreihe 
where lower(name) like '%tuppersex%';

select count(order_number)
from user_bsevilla.v_orders
where erid in (2471671,2559779,2559737,2559736,1280965,2471632);

select distinct ku.id, ku.name artist_name--, er.name eventseries_name
from zone_country_es.v_c_tcdb__dbo__kuenstler ku
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link ek on ek.ku_id = ku.id
left join zone_country_es.v_c_tcdb__dbo__eventreihe er on er.id = ek.er_id
where (lower(er.name) like lower('%Hombres son de Marte%')
or lower(er.name) like lower('%Cable Rojo%')
or lower(er.name) like lower('%Lo Tuyo y Lo M%')
or lower(er.name) like lower('%Bajo terapia%')
or lower(er.name) like lower('%Ilustres Ignorantes%')
or lower(er.name) like lower('%Espinete no existe%')
or lower(er.name) like lower('%De Caperucita a Loba en solo seis%')
or lower(er.name) like lower('%contador del amor%')
or lower(er.name) like lower('%madre que me pari%')
or lower(er.name) like lower('%amor sigue en el aire%')
or lower(er.name) like lower('%Follamigas%')
or lower(er.name) like lower('%Dos hombres solos%')
or lower(er.name) like lower('%Compadres para Siempre%')
or lower(er.name) like lower('%El Intercambio%')
or lower(er.name) like lower('%50 Sombras de Andreu%')
or lower(er.name) like lower('%Celestina%')
or lower(er.name) like lower('%ncipe y la corista%')
or lower(er.name) like lower('%La comedia salv%')
or lower(er.name) like lower('%Orgasmos%')
or lower(er.name) like lower('%SHHH%')
or lower(er.name) like lower('%Divorcio a la esp%')
or lower(er.name) like lower('%amor sigue en el aire%')
or lower(er.name) like lower('%Ocho apellidos andaluces%')
or lower(er.name) like lower('%chicas del zapping%')
or lower(er.name) like lower('%Estr�genos%')
or lower(er.name) like lower('%machos verdes fritos%')
or lower(er.name) like lower('%Ding Dong%')
or lower(er.name) like lower('%Tinder Sorpresa%')
or lower(er.name) like lower('%El gran despipote%')
or lower(er.name) like lower('%Soy de pura madre%')
or lower(er.name) like lower('%MI PADRE FLIPA%')
or lower(er.name) like lower('%El amor est� en el aire%')
or lower(er.name) like lower('%POR LOS PELOS%')
or lower(er.name) like lower('%Humor de transmis%'))
order by 2
;


-- mailing billy

select * from zone_country_es.v_c_dama_csr where mailing_name like '%COM_MUSL_20190828_BIILY_PROMO_%'

-- camp id 8221 action id 8441
-- target groups: 136915 136900
-- mailing id 274345593038 274345593034

select ci.customer_number,
	   csr.mailing_name,
	   h.artist_name,
	   count(distinct mo.pii_userid)/count(distinct mr.pii_userid)
from zone_country_es.v_c_optf__comb__mailingrecipients mr
left join zone_country_es.v_c_optf__comb__opens mo on (mr.pii_userid = mo.pii_userid and mr.mailingid = mo.mailingid)
left join zone_country_es.v_c_dama_csr csr on csr.mailing_id = mr.mailingid
left join user_bsevilla.v_users ci on ci.pii_customer_email = mr.pii_userid
left join (
			select distinct oh.customer_number as customer_number, k.name as artist_name
			from user_bsevilla.v_orders oh
			left join zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link ek on ek.er_id = oh.erid
			left join zone_country_es.v_c_tcdb__dbo__kuenstler k on ek.ku_id = k.id
			where ek.ku_id in (493584,533256,503174,513231,494258,495854,543199,504667)
			) h on h.customer_number = ci.customer_number
where csr.action_id = 8441
and mr.pii_userid is not null
an
group by 1,2,3
order by 1
;

select * from user_bsevilla.v_users where customer_number = 950959654;

-- KNH 696996,696937, 694637
-- ENF 831696, 735003, 348326
--

select count(distinct customer_number), gender
from user_bsevilla.v_users -- 3818677
where 1=1
and leading_system in ('WEB') -- 2289987
and last_interaction > '2016-09-11' -- 3394678 -- 1962738
--and last_order_date > '2016-09-11' -- 2460155 -- 1417192
group by 2
;

-- knh etc

select 
	case when (iohs.web_first_order_date > '2019-09-01' or iohs.web_first_order_date is null) and ci.last_interaction > '2016-09-01' then
		'i_knh'
	when iohs.web_first_order_date > iohk.knh_first_order_date and ci.last_interaction > '2016-09-01' then
		'i_knh->web'
	else 'other' end as i_status,
	case when ci.last_interaction > '2016-09-01' and ci.last_interaction < '2016-09-13' then
		'f_dead'
	when fohk.knh_first_order_date > '2019-09-01' and fohk.knh_first_order_date < '2019-09-13' then
		'f_new_knh'
	when (fohs.web_first_order_date > '2019-09-13' or fohs.web_first_order_date is null) and ci.last_interaction > '2016-09-13' then
		'f_still_knh'
	when fohs.web_first_order_date > fohk.knh_first_order_date and fohs.web_first_order_date < '2019-09-01' and ci.last_interaction > '2016-09-13' then
		'f_knh->web'
	when fohs.web_first_order_date > fohk.knh_first_order_date and fohs.web_first_order_date > '2019-09-01' and fohs.web_first_order_date < '2019-09-13' and ci.last_interaction > '2016-09-13' then
		'f_conv_knh->web'
	else 'other' end as f_status,
	count(kc.customer_id) as 'count'
	--kc.customer_id as customer_id
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where order_date < '2019-09-01'
		        ) 
		group by customer_number
		) iohs on iohs.customer_number = ci.customer_number
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        where order_date < '2019-09-01'
		        )
		group by customer_id
		) iohk on iohk.customer_id = kc.customer_id	
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        where order_date < '2019-09-13'
		        ) 
		group by customer_number
		) fohs on fohs.customer_number = ci.customer_number
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        where order_date < '2019-09-13'
		        )
		group by customer_id
		) fohk on fohk.customer_id = kc.customer_id	--)	
group by 1,2
;

select 
	case when coalesce(ioh.order_cnt, 0) = 1 and ci.last_interaction > '2016-09-01' then 
		'i_otb'
	when coalesce(ioh.order_cnt, 0) > 1 and ci.last_interaction > '2016-09-01' then 
		'i_more'
	when coalesce(ioh.order_cnt, 0) = 0 and ci.last_interaction > '2016-09-01' then 
		'i_zero' 
	else 'other' end as i_status,
	case when ci.last_interaction > '2016-09-01' and ci.last_interaction < '2016-09-13' then 
		'f_dead'
	when coalesce(foh.order_cnt, 0) = 1 and ci.last_interaction > '2016-09-13' then
		case when ci.registration_date < '2019-09-01' then
			'f_otb'
		else 'f_new_otb' end
	when coalesce(foh.order_cnt, 0) > 1 and ci.last_interaction > '2016-09-13' then 
		'f_more'
	when coalesce(foh.order_cnt, 0) = 0 and ci.last_interaction > '2016-09-13' then 
		'f_zero' 
	else 'other' end as f_status,
	count(ci.customer_number)
from user_bsevilla.v_users ci
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-09-01'
		group by 1
		) ioh on ioh.customer_number = ci.customer_number
left join
		(select 
			customer_number, 
			count(order_number) as order_cnt
		from user_bsevilla.v_orders
		where order_date < '2019-09-13'
		group by 1
		) foh on foh.customer_number = ci.customer_number
where ci.leading_system in ('WEB','EVI')
group by 1,2
order by 1,2
;

select ist.i_status, 
	   case when b.f_status = 0 then 'f_inactive' else 'f_active' end as f_status, 
	   avg(days_between(ci.last_interaction, ci.registration_date)) as lifetime,
	   count(ci.customer_number) as 'count'
from user_bsevilla.v_users ci
left join (
		select a.customer_number as customer_number, min(a.i_status) as i_status
		from (
				select ci.customer_number as customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when (ci.first_order_date < '2019-08-01'and nl.subscription_date < '2019-08-01' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-08-01' and ci.last_interaction > '2016-08-01') then
					  	1 --'i_buyer' -- buyer subscriber pero no pasa nada 
					when ci.first_order_date < '2019-08-01' and ci.last_interaction > '2016-08-01' then
						1 --'i_buyer'
					when nl.subscription_date < '2019-08-01' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-08-01' and ci.last_interaction > '2016-08-01' then 
						2 --'i_subscriber'
					when (ci.first_order_date > '2019-08-01' or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > '2016-08-01' then
						3 --'i_visitor'
					else 100 end as i_status
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) a on a.customer_number = ci.customer_number
left join (
		select a.customer_number, max(a.f_status) as f_status
		from (
				select ci.customer_number,
					case when (coalesce(olast.last_order_date,'1900-01-01 00:00:00.0') > add_months('2019-08-01', -6)
							or coalesce(nlast.last_subscription_Date, '1900-01-01 00:00:00.0') > add_months('2019-08-01', -6)
							or coalesce(ci.last_open_date_newsletter, '1900-01-01 00:00:00.0') > add_months('2019-08-01', -3)) then
						1
					else 0 end as f_status
				from user_bsevilla.v_users ci
				left join ( 
							select customer_number, max(subscription_date) as last_subscription_date
							from user_bsevilla.v_newsletters
							where subscription_date < '2019-08-01'
							group by 1	
						  ) nlast on nlast.customer_number = ci.customer_number
				left join ( 
							select customer_number, max(order_date) as last_order_date
							from user_bsevilla.v_orders
							where order_date < '2019-08-01'
							group by 1	
						  ) olast on olast.customer_number = ci.customer_number	  
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) b on b.customer_number = ci.customer_number
left join user_bsevilla.v_dim_i_status ist on a.i_status = ist.i_status_id
where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
and not (a.i_status = 100 and b.f_status = 100)
group by 1,2
;


select 
		/*case when zip_code like '29___' then 'm�laga'
		when zip_code like '03___' then 'alicante'
		when zip_code like '39___' then 'santander' end as ciudad,
		entry_product,
		first_event_name,*/
		count(distinct customer_number) as usuarios_activos
from user_bsevilla.v_users
where (zip_code like '39___'
or zip_code like '29___' 
or zip_code like '03___')
and last_order_date > '2016-09-18'
and entry_product = 'Musical'
--group by 1,2,3
--order by 1,2,3
;

/* julia_juliez	952661374 - 2
susana_susanez	952661392 - 1
lucas_luquez	952661377 - 2  */

select distinct benutzername, kundennummer 
from zone_country_es.v_c_tcdb__dbo__kunde
where benutzername in ('lucas_luquez', 'julia_juliez', 'susana_susanez')
;

select customer_number, reco_level from user_bsevilla.v_users where customer_number in (952661374,952661392,952661377);

select order_cnt, reco_level
from user_bsevilla.v_users
where leading_system in ('WEB', 'EVI')
and last_interaction > '2016-09-18'
;
