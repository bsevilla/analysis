-- cuantas personas de v_users web+evi tienen m�s de 10 tickets para el mismo evento?

select reco_level, count(distinct u.customer_number)
from user_bsevilla.v_users u
left join (
			select customer_number, max(ords) as max_ords
			from (
					select customer_number, evid, count(distinct order_number) as ords
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o on o.customer_number = u.customer_number
where u.leading_system in ('WEB','EVI')
and last_interaction > '2016-09-20'
and o.max_ords < 4
and u.suffix not like '%entrada%'
and u.suffix not like '%ticket%'
and u.suffix not like '%viajes%'
and u.suffix not like '%adventure%'
group by 1
order by 1
;


select case when o.max_ords <= 10 and o2.max_tx <= 15 then 'ok'
		else 'fraud' end as fraud_excl,
		count(u.customer_number)
from user_bsevilla.v_users u
left join (
			select pii_customer_email, max(ords) as max_ords
			from (
					select pii_customer_email, evid, count(distinct order_number) as ords
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o on o.pii_customer_email = u.pii_customer_email
left join (
			select pii_customer_email, max(tx) as max_tx
			from (
					select pii_customer_email, evid, sum(ticket_number) as tx
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o2 on o2.pii_customer_email = u.pii_customer_email
where u.leading_system in ('WEB','EVI')
and last_interaction > '2016-09-24'
and u.suffix not like '%entrada%'
and u.suffix not like '%ticket%'
and u.suffix not like '%viajes%'
and u.suffix not like '%adventure%'
and u.last_open_date_newsletter is not null
group by 1
order by 1
;

-- realmente abren menos?

select case when o.max_ords <= 3 and o2.max_tx <= 15 then 'ok' else 'fraud' end as fraud_excl,
		u.reco_level, 
		count(distinct u.customer_number) user_count,
		avg(coalesce(ci.open_rate_newsletter_360,0)) avg_or_360,
		avg(coalesce(ci.click_rate_newsletter_360,0)) avg_ctr_360,
		sum(oh.orders) orders
from user_bsevilla.v_users u
left join zone_country_es.v_c_dama_ci ci on u.customer_number = ci.customer_number
left join (
			select pii_customer_email, max(ords) as max_ords
			from (
					select pii_customer_email, evid, count(distinct order_number) as ords
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o on o.pii_customer_email = u.pii_customer_email
left join (
			select pii_customer_email, max(tx) as max_tx
			from (
					select pii_customer_email, evid, sum(ticket_number) as tx
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o2 on o2.pii_customer_email = u.pii_customer_email
left join ( select kundennummer, count(ordernummer) orders
			from zone_country_es.v_c_tcdb__dbo__order_header
			where (referer_info like '%TGP%' or referer_info like '%TGS%')
			group by 1
		  ) oh on oh.kundennummer = u.customer_number
where u.leading_system in ('WEB','EVI')
and u.last_interaction > '2016-09-24'
and u.suffix not like '%entrada%'
and u.suffix not like '%ticket%'
and u.suffix not like '%viajes%'
and u.suffix not like '%adventure%'
and u.suffix not like '%stage%'
group by 1,2 
order by 1,2
;

select distinct referer_info
from zone_country_es.v_c_tcdb__dbo__order_header
where orderdatum > '2019-01-01'
and referer_info like '%TGP%'
;



select distinct u.suffix from user_bsevilla.v_users u 
where (u.suffix like '%entrada%'
or u.suffix like '%ticket%'
or u.suffix like '%viajes%'
or u.suffix like '%adventure%');
