-- cross sellers
select 
	ci.customer_number as customer_number,
	ci.entry_product,
	ci.first_event_name,
	ohk.knh_first_order_date,
	ohk.knh_last_order_date,
	ci.first_order_date,
	abs(days_between(ohk.knh_first_order_date,ci.first_order_date)) as conversion_days,
	g.film_name,
	g.genre_name,
	g2.film_name as film_name_last,
	g2.genre_name as genre_name_last,
	ci.age_range,
	ci.gender
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date,
		        max(case when rnAsc = 1 then order_id end) as knh_first_order_id,
		        max(case when rnDesc = 1 then order_date end) as knh_last_order_date,
		        max(case when rnDesc = 1 then order_id end) as knh_last_order_id
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc,
		                row_number() over (partition by customer_id order by order_date desc) as rnDesc,
		                order_id, order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        )
		group by customer_id
		) ohk on ohk.customer_id = kc.customer_id
left join (
			select distinct op.order_id as order_id, 
					da.artist_name as film_name, 
					dg.genre_name as genre_name
			from zone_country_es.v_c_kinoheld__fact_order_position op
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__dim_artist da on ea.artist_id = da.artist_id
			left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ag.artist_id = ea.artist_id
			left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
			) g on g.order_id = ohk.knh_first_order_id
left join (
			select distinct op.order_id as order_id, 
					da.artist_name as film_name, 
					dg.genre_name as genre_name
			from zone_country_es.v_c_kinoheld__fact_order_position op
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__dim_artist da on ea.artist_id = da.artist_id
			left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ag.artist_id = ea.artist_id
			left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
			) g2 on g2.order_id = ohk.knh_last_order_id
where ci.first_order_date > ohk.knh_first_order_date and ci.last_interaction > '2016-08-21'
and ci.leading_system in ('WEB', 'EVI')
--limit 10
--group by 2
;

-- random sample

select 
	ci.customer_number as customer_number,
	--ci.entry_product,
	ohk.knh_first_order_date,
	--ci.first_order_date,
	--abs(days_between(ohk.knh_first_order_date,ci.first_order_date)) as conversion_days,
	g.film_name,
	g.genre_name,
	g2.film_name as film_name_last,
	g2.genre_name as genre_name_last,ci.age_range,
	ci.gender
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date,
		        max(case when rnAsc = 1 then order_id end) as knh_first_order_id,
		        max(case when rnDesc = 1 then order_date end) as knh_last_order_date,
		        max(case when rnDesc = 1 then order_id end) as knh_last_order_id
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc,
		                row_number() over (partition by customer_id order by order_date desc) as rnDesc,
		                order_id, order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        )
		group by customer_id
		) ohk on ohk.customer_id = kc.customer_id
left join (
			select distinct op.order_id as order_id, 
					da.artist_name as film_name, 
					dg.genre_name as genre_name
			from zone_country_es.v_c_kinoheld__fact_order_position op
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__dim_artist da on ea.artist_id = da.artist_id
			left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ag.artist_id = ea.artist_id
			left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
			) g on g.order_id = ohk.knh_first_order_id
left join (
			select distinct op.order_id as order_id, 
					da.artist_name as film_name, 
					dg.genre_name as genre_name
			from zone_country_es.v_c_kinoheld__fact_order_position op
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__dim_artist da on ea.artist_id = da.artist_id
			left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ag.artist_id = ea.artist_id
			left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
			) g2 on g2.order_id = ohk.knh_last_order_id
where ci.last_interaction > '2016-08-21'
and leading_system = 'KNH'
order by rand()
limit 10
--group by 2
; 


-- todas las compras de todos

select 
	ci.customer_number as customer_number,
	ci.entry_product,
	ci.first_event_name,
	ohk.knh_first_order_date,
	ohk.knh_last_order_date,
	ci.first_order_date,
	abs(days_between(ohk.knh_first_order_date,ci.first_order_date)) as conversion_days,
	g.film_name,
	g2.film_name as film_name_last,
	da.artist_name as movie,
	ci.age_range,
	ci.gender
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date,
		        max(case when rnAsc = 1 then order_id end) as knh_first_order_id,
		        max(case when rnDesc = 1 then order_date end) as knh_last_order_date,
		        max(case when rnDesc = 1 then order_id end) as knh_last_order_id
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc,
		                row_number() over (partition by customer_id order by order_date desc) as rnDesc,
		                order_id, order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        )
		group by customer_id
		) ohk on ohk.customer_id = kc.customer_id
left join (
			select distinct op.order_id as order_id, 
					da.artist_name as film_name, 
					dg.genre_name as genre_name
			from zone_country_es.v_c_kinoheld__fact_order_position op
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__dim_artist da on ea.artist_id = da.artist_id
			left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ag.artist_id = ea.artist_id
			left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
			) g on g.order_id = ohk.knh_first_order_id
left join (
			select distinct op.order_id as order_id, 
					da.artist_name as film_name, 
					dg.genre_name as genre_name
			from zone_country_es.v_c_kinoheld__fact_order_position op
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__dim_artist da on ea.artist_id = da.artist_id
			left join zone_country_es.v_c_kinoheld__link_artist_genre ag on ag.artist_id = ea.artist_id
			left join zone_country_es.v_c_kinoheld__dim_genre dg on dg.genre_id = ag.genre_id
			) g2 on g2.order_id = ohk.knh_last_order_id
inner join (
			select oh.customer_id, ea.artist_id as artist_id
			from zone_country_es.v_c_kinoheld__fact_order oh
			left join zone_country_es.v_c_kinoheld__fact_order_position op on oh.order_id = op.order_id
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id 
			where ea.artist_id in (4228,5128,8080,5798)
) ao on ao.customer_id = kc.customer_id
left join zone_country_es.v_c_kinoheld__dim_artist da on ao.artist_id = da.artist_id
where ci.first_order_date > ohk.knh_first_order_date and ci.last_interaction > '2016-08-21'
and ci.leading_system in ('WEB', 'EVI')
limit 10
--group by 2
;

select oh.customer_id, ea.artist_id
			from zone_country_es.v_c_kinoheld__fact_order oh
			left join zone_country_es.v_c_kinoheld__fact_order_position op on oh.order_id = op.order_id
			left join zone_country_es.v_c_kinoheld__dim_event de on op.event_id = de.event_id
			left join zone_country_es.v_c_kinoheld__link_event_artist ea on ea.event_id = de.event_id 
			where ea.artist_id in (4228,5128,8080,5798) limit 10;