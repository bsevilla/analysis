with convs as (
select 
	ci.customer_number as customer_number,
	iohs.web_first_order_date as first_order_date_web
from zone_country_es.v_c_kinoheld__dim_customer kc
left join user_bsevilla.v_users ci on ci.pii_customer_email = kc.pii_customer_email
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as web_first_order_date
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date
		        from user_bsevilla.v_orders
		        ) 
		group by customer_number
		) iohs on iohs.customer_number = ci.customer_number
left join --zone_country_es.v_c_kinoheld__fact_order oh on dc.customer_id = oh.customer_id
		(select  customer_id,
		        max(case when rnAsc = 1 then order_date end) as knh_first_order_date
		from    (
		        select  customer_id,
		                row_number() over (partition by customer_id order by order_date) as rnAsc, 
		                order_date
		        from zone_country_es.v_c_kinoheld__fact_order
		        )
		group by customer_id
		) iohk on iohk.customer_id = kc.customer_id	
where iohs.web_first_order_date > iohk.knh_first_order_date
and iohs.web_first_order_date > '2019-01-01'
and ci.last_interaction > '2016-11-11'
and ci.leading_system in ('WEB', 'EVI')
)
select to_char(convs.first_order_date_web, 'YYYYMM'), convs.customer_number, count(distinct oh.order_number), sum(oh.ticket_number)
from convs 
left join user_bsevilla.v_orders oh on convs.customer_number = oh.customer_number 
--where oh.orderdatum > convs.first_order_date_web
group by 1,2
order by 1
;
