select 
	case when lower(mailing_type_name) like '%insider%' then
		'insider'
	when lower(mailing_type_name) like '%regio%' then
		'regio'
	when lower(mailing_type_name) like '%special%' then
		'special'
	when lower(mailing_type_name) like '%cinema%' then
		'cinema'
	when lower(mailing_type_name) like '%similar%' then
		'similar'
	when lower(mailing_type_name) like '%birthday%' then
	  'birthday'
	when lower(mailing_type_name) like '%abandonment%' then
	  'cart abandonment'
	else 'rest'
	end as mailing_type,
	mailing_name,
	to_char(send_date, 'YYYYMM') as send_month,
	/*avg(opt_net) as opt_net,
	avg(u_opens)/avg(opt_net) as "OR",
	avg(u_clicks)/avg(opt_net) as ctr,
	avg(unsubscriptions)/avg(opt_net) as unsubs,*/
	sum(crm_tix) as crm_tix,
	sum(crm_tix_direct) as crm_tix_direct,
	sum(crm_tix_indirect) as crm_tix_indirect,
	sum(combined_tix) as combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-09'
and send_date < '2019-01-01'
/*and (lower(mailing_type_name) like '%insider%'
or lower(mailing_type_name) like '%regio%'*/
and lower(mailing_type_name) like '%special%'
/*or lower(mailing_type_name) like '%cinema%'
or lower(mailing_type_name) like '%similar%')*/
group by 1,2,3
order by 1,3,2
;


select count(distinct u.customer_number)
from user_bsevilla.v_users u
left join zone_country_es.v_c_dama_ci ci on ci.first_order_date between u.first_order_date and u.last_order_date
limit 10;
--acos(sin(ctx.lat1*3.141593/180)*sin(ctx.lat2*3.141593/180) + cos(ctx.lat1*3.141593/180)*cos(ctx.lat2*3.141593/180)*cos(ctx.lon2*3.141593/180-ctx.lon1*3.141593/180) )*6371


select 
		mailing_type_name,
		mailing_name,
		to_char(send_date, 'YYYYMM') as send_month,
		crm_tix,
		crm_tix_direct,
		crm_tix_indirect,
		combined_tix
from zone_country_es.v_c_dama_csr 
where 1=1
and send_date > '2018-01-01'
and send_date < '2019-01-01'
and mailing_type_name = 'Special promos'
and crm_tix > 0
order by 3,2
;

select benutzername, kundennummer from zone_country_es.v_c_tcdb__dbo__kunde where benutzername in ('lucas_luquez','julia_juliez','susana_susanez','cristina_cristinez') limit 10;
select * from user_bsevilla.v_orders where customer_number = 952661388;
select a.benutzername, u.reco_level 
from user_bsevilla.v_users u 
left join (select benutzername, kundennummer from zone_country_es.v_c_tcdb__dbo__kunde where benutzername in ('lucas_luquez','julia_juliez','susana_susanez','cristina_cristinez')) a on a.kundennummer = u.customer_number
where u.customer_number in (952661374,952661388,952661392,952661377);


select * from zone_country_es.v_fact_order_base where first_order_flg = 1 limit 10;


select * from zone_country_es.v_c_tcdb__dbo__order_header limit 100;


select distinct referer_info from zone_country_es.v_c_tcdb__dbo__order_header where orderdatum > '2019-01-01' limit 100;


select cp.provincia, count(distinct u.customer_number)
from user_bsevilla.v_users u
left join user_bsevilla.v_newsletters nl on nl.customer_number = u.customer_number
left join user_bsevilla.v_dim_cp cp on u.zip_code = cp.cp
where 1=1
--and u.lifecycle_status = 'Interested' 
and u.order_cnt = 0
and nl.newsletter_type is not null
--and nl.newsletter_category is not null
and u.leading_system in ('WEB', 'EVI')
and u.first_order_date is null
group by 1
order by 2 desc
;

select count(distinct u.customer_number), nl.newsletter_type
from user_bsevilla.v_users u
left join user_bsevilla.v_newsletters nl on nl.customer_number = u.customer_number
where 1=1
and u.order_cnt = 0
and nl.newsletter_type is not null
and nl.newsletter_category is not null
and u.leading_system in ('WEB', 'EVI')
and u.first_order_date is null
group by 2
;

select case when nl.newsletter_category is null then nl.newsletter_type else nl.newsletter_category end as nltype, count(distinct ci.customer_number)
from user_bsevilla.v_users ci
left join (
		select a.customer_number as customer_number, min(a.i_status) as i_status
		from (
				select ci.customer_number as customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when (ci.first_order_date < '2019-10-07'and nl.subscription_date < '2019-10-07' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-10-07' and ci.last_interaction > '2016-10-07') then
					  	1 --'i_buyer' -- buyer subscriber pero no pasa nada 
					when ci.first_order_date < '2019-10-07' and ci.last_interaction > '2016-10-07' then
						1 --'i_buyer'
					when nl.subscription_date < '2019-10-07' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-10-07' and ci.last_interaction > '2016-10-07' then 
						2 --'i_subscriber'
					when (ci.first_order_date > '2019-10-07' or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > '2016-10-07' then
						3 --'i_visitor'
					else 100 end as i_status
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) a on a.customer_number = ci.customer_number
left join user_bsevilla.v_dim_i_status ist on a.i_status = ist.i_status_id
left join user_bsevilla.v_newsletters nl on nl.customer_number = ci.customer_number
left join user_bsevilla.v_dim_cp cp on ci.zip_code = cp.cp
where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
and not (a.i_status = 100)
and ist.i_status = 'i_subscriber'
--and nl.newsletter_category is not null
--and nl.newsletter_type = 'Infomailing' --'General NL' --
group by 1
order by 2 desc
;

select distinct newsletter_type from user_bsevilla.v_newsletters;

select to_char(ob.event_date, 'YYYYMMDD') as ev_date, ob.artist_id, count(distinct ob.customer_number)
from zone_country_es.v_fact_order_base ob
left join user_bsevilla.v_users u on u.customer_number = ob.customer_number
where ob.event_date > '2019-10-09' 
and ob.event_date < '2019-10-12'
and ob.artist_id in (565075, 565614)
and coalesce(ob.order_status,100) in (100,300)
and coalesce(ob.order_delivery_status,2) >= 0
and coalesce(ob.order_delivery_status,2) != 9
and coalesce(ob.product_type_id,1) = 1
and coalesce(ob.artist_rank,1) = 1
and coalesce(ob.genre_rank,1) = 1
and coalesce(ob.voucher_rank,1) = 1
and ob.ticket_rank = 1
and ob.position_rank = 1
and u.honesty_status > 0
group by 1,2
order by 1,2
;


select * from zone_country_es.v_c_tcdb__dbo__kuenstler where name like '%Messi%';


select distinct customer_city_name
from zone_country_es.v_dc_dama_ci
where lower(customer_city_name) like '%madrid%'
;


select mailing_name,
	send_date,
	opt_net,
	100*u_opens/opt_net as "OR",
	100*u_clicks/opt_net as ctr,
	100*unsubscriptions/opt_net as unsubs,
	crm_tix as crm_tix,
	crm_tix_direct as crm_tix_direct,
	combined_tix as combined_tix
from zone_country_es.v_c_dama_csr 
where lower(mailing_name) like '%react%'
--and opt_net > 100
order by send_date;

select source_name, a.artist_name, count(distinct ob.order_number)
from zone_country_es.v_fact_order_base ob
left join zone_country_es.v_c_dama_artist a on a.artist_id = ob.artist_id
where ob.order_date > '2019-10-01' 
and coalesce(ob.order_status,100) in (100,300)
and coalesce(ob.order_delivery_status,2) >= 0
and coalesce(ob.order_delivery_status,2) != 9
and coalesce(ob.product_type_id,1) = 1
and coalesce(ob.artist_rank,1) = 1
and coalesce(ob.genre_rank,1) = 1
and coalesce(ob.voucher_rank,1) = 1
and ob.ticket_rank = 1
and ob.position_rank = 1
group by 1,2
order by 3 desc
limit 100
;

select to_char(ob.event_date, 'YYYYMMDD') as event_date, a.artist_name, count(distinct ob.order_number)
from zone_country_es.v_fact_order_base ob
left join zone_country_es.v_c_dama_artist a on a.artist_id = ob.artist_id
left join zone_country_es.v_dc_c_bridge_customer_newsletter_base cn on ob.customer_number = cn.customer_number
--where ob.order_date > '2019-10-01' 
where 1=1
and coalesce(ob.order_status,100) in (100,300)
and coalesce(ob.order_delivery_status,2) >= 0
and coalesce(ob.order_delivery_status,2) != 9
and coalesce(ob.product_type_id,1) = 1
and coalesce(ob.artist_rank,1) = 1
and coalesce(ob.genre_rank,1) = 1
and coalesce(ob.voucher_rank,1) = 1
and ob.ticket_rank = 1
and ob.position_rank = 1
and ob.artist_id in (565075,565614)
and cn.newsletter_category_id in (5)
and cn.newsletter_status = 1
and ob.event_date > '2019-10-10'
and ob.event_date < '2019-10-15'
group by 1,2
order by 1,2
limit 100
;

-- first subscription subscribers
select nl.first_newsletter_category, nl.first_newsletter_type, count(distinct ci.customer_number)
from user_bsevilla.v_users ci
left join (
		select a.customer_number as customer_number, min(a.i_status) as i_status
		from (
				select ci.customer_number as customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when (ci.first_order_date < '2019-10-15'and nl.subscription_date < '2019-10-15' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-10-15' and ci.last_interaction > '2016-10-15') then
					  	1 --'i_buyer' -- buyer subscriber pero no pasa nada 
					when ci.first_order_date < '2019-10-15' and ci.last_interaction > '2016-10-15' then
						1 --'i_buyer'
					when nl.subscription_date < '2019-10-15' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-10-15' and ci.last_interaction > '2016-10-15' then 
						2 --'i_subscriber'
					when (ci.first_order_date > '2019-10-15' or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > '2016-10-15' then
						3 --'i_visitor'
					else 100 end as i_status
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) a on a.customer_number = ci.customer_number
left join user_bsevilla.v_dim_i_status ist on a.i_status = ist.i_status_id
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then subscription_date end) as first_subscription_date,
		        max(case when rnAsc = 1 then newsletter_type end) as first_newsletter_type,
		        max(case when rnAsc = 1 then newsletter_category end) as first_newsletter_category
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by subscription_date) as rnAsc, 
		                subscription_date, newsletter_type, newsletter_category
		        from user_bsevilla.v_newsletters
		        ) 
		group by customer_number
		) nl on nl.customer_number = ci.customer_number
left join user_bsevilla.v_dim_cp cp on ci.zip_code = cp.cp
where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
and ist.i_status = 'i_subscriber'
--and nl.newsletter_category is not null
--and nl.newsletter_type = 'Infomailing' --'General NL' --
group by 1,2
order by 3 desc
;


select * from zone_country_es.v_c_tcdb__dbo__kunde
where kundenemail in ('pruebalocalizacion@gmail.com', 'pruebalocalizacion2@gmail.com');

select * from zone_country_es.v_c_dama_ci
where customer_number in (952816607, 952816619);

select 
		--case when k.kundenplz is null then 'null' else 'not null' end as loc_data, count(k.sk)
		case when k.customer_federal_region is null then 'null' else 'not null' end as loc_data, 
		case when order_cnt is null then 'null' else 'not null' end as ords, 
		count(distinct k.customer_number)
		/*k.customer_number, 
		case when k.customer_city_name is null then 'null' else 'not null' end as loc_data, 
		ci.order_cnt*/
		--k.customer_affiliate, count(distinct k.customer_number)
from zone_country_es.v_c_dama_ci k
left join (
			select customer_number, count(distinct order_number) as order_cnt
			from zone_country_es.v_fact_order_base ob
			where source_name = 'WEB'
			and coalesce(ob.order_status,100) in (100,300)
			and coalesce(ob.order_delivery_status,2) >= 0
			and coalesce(ob.order_delivery_status,2) != 9
			and coalesce(ob.product_type_id,1) = 1
			and coalesce(ob.artist_rank,1) = 1
			and coalesce(ob.genre_rank,1) = 1
			and coalesce(ob.voucher_rank,1) = 1
			and ob.ticket_rank = 1
			and ob.position_rank = 1
			group by 1
) ci on k.customer_number = ci.customer_number
where k.reference_date > '2016-10-15'
--and k.customer_city_name is null
--and order_cnt is not null
group by 1,2
order by 3 desc
;

select kundenort from zone_country_es.v_c_tcdb__dbo__kunde where kundennummer = 951290916;
select customer_city_name from zone_country_es.v_c_dama_ci where customer_number = 951290916;

select case when cei.customer_city_name is null then 'null' else 'not null' end as cnisnull, count(distinct ci.customer_number)
from user_bsevilla.v_users ci
left join (
		select a.customer_number as customer_number, min(a.i_status) as i_status
		from (
				select ci.customer_number as customer_number,--count(ci.customer_number), count(distinct ci.customer_number), --
					case when (ci.first_order_date < '2019-10-07'and nl.subscription_date < '2019-10-07' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-10-07' and ci.last_interaction > '2016-10-07') then
					  	1 --'i_buyer' -- buyer subscriber pero no pasa nada 
					when ci.first_order_date < '2019-10-07' and ci.last_interaction > '2016-10-07' then
						1 --'i_buyer'
					when nl.subscription_date < '2019-10-07' and coalesce(nl.unsubscription_date, '2900-01-01 00:00:00.0') > '2019-10-07' and ci.last_interaction > '2016-10-07' then 
						2 --'i_subscriber'
					when (ci.first_order_date > '2019-10-07' or ci.first_order_date is null) and nl.subscription_date is null and ci.last_interaction > '2016-10-07' then
						3 --'i_visitor'
					else 100 end as i_status
				from user_bsevilla.v_users ci
				left join user_bsevilla.v_newsletters nl on ci.customer_number = nl.customer_number
				where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
				) a
		group by 1) a on a.customer_number = ci.customer_number
left join user_bsevilla.v_dim_i_status ist on a.i_status = ist.i_status_id
left join user_bsevilla.v_newsletters nl on nl.customer_number = ci.customer_number
left join user_bsevilla.v_dim_cp cp on ci.zip_code = cp.cp
left join zone_country_es.v_c_dama_ci cei on ci.customer_number = cei.customer_number
where ci.leading_system <> 'ENL' -- in ('WEB', 'EVI')
and not (a.i_status = 100)
and ist.i_status = 'i_subscriber'
--and nl.newsletter_category is not null
--and nl.newsletter_type = 'Infomailing' --'General NL' --
group by 1
order by 2 desc
;

select
		mailing_type_name,
		mailing_name,
		to_char(send_date, 'YYYYMM') as send_month,
		crm_tix,
		crm_tix_direct,
		crm_tix_indirect,
		combined_tix
from zone_country_es.v_c_dama_csr
where 1=1
and send_date > '2016-01-01'
and send_date < '2019-01-01'
and mailing_type_name = 'Special promos'
and crm_tix > 0
order by 3,2
;


select insert_date, kundenvorname, kundennachname
from zone_country_es.v_c_tcdb__dbo__kunde 
where insert_date > '2019-02-04'
and insert_date < '2019-02-10'
order by 1
limit 100;

select * from zone_country_es.v_c_tcdb__dbo__event where ku_id = 496335;

select count(distinct ob.customer_number)
from zone_country_es.v_fact_order_base ob
left join zone_country_es.v_dc_c_bridge_customer_newsletter_base cn
on ob.customer_number = cn.customer_number
where ob.artist_id = 565075
and cn.newsletter_category_id = '3'
and cn.newsletter_status = '1'
;

select distinct ob.order_date
from zone_country_es.v_fact_order_base ob
--left join zone_country_es.v_dc_c_bridge_customer_newsletter_base cn
--on ob.customer_number = cn.customer_number
where ob.artist_id = 565075
order by 1
limit 10;

select  to_char(registration_date, 'YYYYMMDD') as registration_date,
		count(distinct customer_number)
from zone_country_es.v_c_dama_ci u
left join user_bsevilla.v_dim_cp cp on u.customer_zip_code = cp.cp
where u.leading_system in ('WEB', 'EVI')
and (lower(u.customer_city_name) like '%bcn%' or
lower(u.customer_city_name) like '%barcelona%' or
lower(u.customer_city_name) like '%barna%' or
cp.provincia = 'Barcelona'
)
and registration_date > '2017-01-01'
group by 1
order by 1
;

select  to_char(u.registration_date, 'YYYYMMDD') as registration_date,
		count(distinct u.customer_number)
from zone_country_es.v_c_dama_ci u
left join user_bsevilla.v_users us on u.customer_number = us.customer_number
left join user_bsevilla.v_dim_cp cp on u.customer_zip_code = cp.cp
where u.leading_system in ('WEB', 'EVI')
and us.first_event_name like '%Messi%'
and u.registration_date > '2017-01-01'
group by 1
order by 1
;

select u.customer_city_name, u.customer_city_name_clean, count(distinct customer_number)
from zone_country_es.v_c_dama_ci u
left join user_bsevilla.v_dim_cp cp on u.customer_zip_code = cp.cp
where cp.provincia = 'Cantabria'
group by 1,2
order by 3 desc
;

select min(order_date)
from zone_country_es.v_fact_order_base ob
left join zone_country_es.v_c_dama_venue v on ob.venue_id = v.venue_id
where artist_id = 494155
--group by 1,2
;

select ev.event_name, count(distinct order_number)
from user_bsevilla.v_users u
left join user_bsevilla.v_dim_cp cp on u.zip_code = cp.cp
left join zone_country_es.v_fact_order_base ob on u.customer_number = ob.customer_number
left join zone_country_es.v_c_dama_event ev on ev.event_id = ob.event_id
where cp.provincia = 'Las Palmas'
and ob.venue_zip not like '35___'
and u.honesty_status > 0
group by 1
order by 2 desc
;


select distinct cp, provincia from user_bsevilla.v_dim_cp where provincia = 'Las Palmas';

select count(distinct customer_number) from user_bsevilla.v_users where last_interaction > '2016-10-21'

select distinct customer_city_name
from zone_country_es.v_c_dama_ci 
where (lower(customer_city_name) like '%barcelona%'
	  or lower(customer_city_name) like '%bcn%' 
	  or lower(customer_city_name) like '%barna%'
	  )
and leading_system in ('WEB', 'EVI');

select count(distinct u.customer_number) 
from zone_country_es.v_fact_order_base ob
left join user_bsevilla.v_users u on u.customer_number = ob.customer_number
where venue_id in (31300010319,45300010319,42900010319,55000010319,20600010319,33100010319,43300010319)
and last_interaction > '2016-10-23'
;

select distinct u.customer_number, ob.order_number, ev.event_name, ob.order_date
from user_bsevilla.v_users u
left join zone_country_es.v_c_dama_ci ci on u.customer_number = ci.customer_number
left join zone_country_es.v_fact_order_base ob on ob.customer_number = u.customer_number
left join zone_country_es.v_c_dama_event ev on ob.event_id = ev.event_id
where honesty_status > 0
and u.leading_system in ('WEB', 'EVI')
and u.last_order_date > '2016-10-23'
and (u.federal_region in ('Catalu�a', 'Catalunya')
and u.order_cnt > 1
or u.zip_code like '08___'
or lower(ci.customer_city_name) like '%barcelona%'
or lower(ci.customer_city_name) like '%barna%'
or lower(ci.customer_city_name) like '%bcn%')
order by 1,4
;

select * from user_bsevilla.v_dim_cp limit 1;

select * from zone_country_es.v_c_dama_ci limit 10;
