select max(order_rec_lng)
from zone_country_es.v_c_dama_ci 
where order_cnt_lng > 0
limit 10;

select 	ci.customer_number,
		coalesce(ci.order_rec_lng, 0) as r,
		coalesce(ci.order_frq_lng, 0) as f,
		coalesce(ci.monetary_value_lng,0) as m,
		coalesce(ci.order_cnt_lng, 0) as order_cnt,
		coalesce(u.entry_product, 'none') as entry_product
from zone_country_es.v_c_dama_ci ci
join user_bsevilla.v_users u on ci.customer_number = u.customer_number
join zone_country_es.v_dc_order oh on ci.customer_number = oh.customer_number
where 1=1
and ci.leading_system in ('EVI','WEB')
limit 10;


select distinct customer_city_name from zone_country_es.v_c_dama_ci 
where lower(customer_city_name) like '%alicante%' 
or lower(customer_city_name) like '%alacant%' 
;

select mr.number, count(mr.mailingid) as mailings, count(o.mailingid) as opens, count(c.mailingid) as clicks
from zone_country_es.v_c_dama_csr csr
left join zone_country_es.v_c_optf__comb__mailingrecipients mr on mr.mailingid = csr.mailing_id
left join zone_country_es.v_c_optf__comb__opens o on o.mailingid = mr.mailingid and o.pii_userid = mr.pii_userid
left join zone_country_es.v_c_optf__comb__clicks c on c.mailingid = o.mailingid and o.pii_userid = c.pii_userid
where lower(csr.mailing_type_name) like '%insider%'
and c.mailingid is not null
group by 1
limit 10;

select * from zone_country_es.v_c_optf__comb__clicks limit 10;


select 	o.order_number,
o.artist_id,
o.artist_name,
o.vertical,
o.ticket_number,
o.order_date,
o.order_total/o.ticket_number as unit_price,
u.customer_number
from user_bsevilla.v_users u
left join user_bsevilla.v_orders o on o.customer_number = u.customer_number
where 1=1
and u.leading_system in ('EVI','WEB')
and o.order_date > '2017-05-01'
limit 10;

select * from zone_country_es.v_dc_order_position limit 10;
