--create or replace view user_bsevilla.v_newsletters as
select 
	ci.customer_number as customer_number, 
	nc.newsletter_category_name as newsletter_type,
	ng.maingenre_name as newsletter_category,
	nl.subscription_date as subscription_date,
	nl.unsubscribe_date as unsubscription_date
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_dc_lnk__customer__newsletter nl on ci.customer_number = nl.customer_number
left join zone_country_es.v_dc_newsletter_category nc on nl.newsletter_category_id = nc.newsletter_category_id
left join zone_country_es.v_dc_genre ng on ng.genre_id = nl.genre_id
where nl.subscription_date is not null
;

select * from zone_country_es.v_dc_genre;



/*create table user_bsevilla.v_dim_i_status(
	i_status_id int,
	i_status varchar(40));*/

/*create table user_bsevilla.v_dim_f_status(
	f_status_id int,
	f_status varchar(40));*/