-- cps
--create or replace table user_bsevilla.v_dim_cp (
									code varchar(10),
									provincia varchar(30),
									pueblo varchar(70),
									cp varchar(10),
									latitud decimal(9,6),
									longitud decimal(9,6),
									poblacion decimal,
									geo geometry(4326)
									
)


--insert into user_bsevilla.v_dim_cp
select code,provincia, pueblo, right(cp, 4) as cp, latitud, longitud, poblacion, geo from user_bsevilla.v_dim_cp
where left(cp, 1) = '0'
;
--insert into user_bsevilla.v_dim_cp
select code,provincia, 'Palmas' as pueblo, '35000' as cp, latitud, longitud, poblacion, first_value(geo) from user_bsevilla.v_dim_cp
where pueblo = 'Las Palmas de Gran Canaria'
group by 1,2,3,4,5,6,7
;
--, -- '35000' as cp
--insert into user_bsevilla.v_dim_cp
select code,provincia, 'Gasteiz' as pueblo, '01000' as cp, latitud, longitud, poblacion, first_value(geo) from user_bsevilla.v_dim_cp
where lower(pueblo) like '%vitoria%'
group by 1,2,3,4,5,6,7
;

select code,provincia, 'Vitoria' as pueblo, '01000' as cp, latitud, longitud, poblacion, first_value(geo) from user_bsevilla.v_dim_cp
where lower(pueblo) like '%vitoria%'
group by 1,2,3,4,5,6,7
;

--insert into user_bsevilla.v_dim_cp
select code,provincia, 'Las Rozas' as pueblo, '28290' as cp, latitud, longitud, poblacion, first_value(geo) from user_bsevilla.v_dim_cp
where lower(pueblo) like '%las rozas de madrid%'
group by 1,2,3,4,5,6,7
;


select * from user_bsevilla.v_zip_code_lookup where length(zip_code) < 5;

select count(*) from user_bsevilla.v_dim_cp;-- where pueblo like '%Vicente%';