-- users table

--create or replace view user_bsevilla.v_users as
--select honesty_status,count(distinct customer_number) from(
select 
	ci.customer_number as customer_number, 
	ci.customer_id,
	ci.leading_system as leading_system,
	case when ard.age_range_name is not null then ard.age_range_name else 'undefined' end as age_range,
	case when gb1.customer_gender <> 'UNDEF' then 
		  lower(gb1.customer_gender)
		when (gb1.customer_gender = 'UNDEF' or gb1.customer_gender is null) and (gb2.customer_gender = 'UNDEF' or gb2.customer_gender is null) then 
		  'undefined'
		else lower(gb2.customer_gender)
		end as gender,
	ci.registration_date as registration_date,
	ci.reco_level as reco_level,
	ci.order_cnt_all as order_cnt,
	ci.last_order_date as last_order_date,
	ci.last_open_date_newsletter as last_open_date_newsletter,
	nl.last_subscription_date as last_subscription_date,
	case when (ci.registration_date is null and ci.last_order_date is null and ci.last_open_date_newsletter is null and nl.last_subscription_date is null) then
		ci.reference_date
		when coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') 
		  and coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		  and coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		  --and coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		  then
		ci.last_order_date
		when coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		 and coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		-- and coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		 then
		ci.last_open_date_newsletter
		when coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') 
		 and coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		-- and coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		 then 
		nl.last_subscription_date
		when coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		 and coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0')
		 --and coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		 then
		ci.registration_date
		/*when coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		 and coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0')
		 and coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		then
		ci.reference_date*/
		else '1900-01-01 00:00:00.0' end as last_interaction,
	ohs.first_order_number as first_order_number,
	case when leading_system in ('KNH','ENF') then
		ci.registration_date
		when leading_system in ('WEB', 'EVI') then
		ohs.first_order_date
		else '2999-01-01 00:00:00.0' end as first_order_date,
	case when leading_system in ('KNH','ENF') then
		NULL
		when leading_system in ('WEB', 'EVI') then
		ohs.first_event_erid
		else 9999999 end as first_event_erid, 
	case when leading_system in ('KNH','ENF') then
		NULL
		when leading_system in ('WEB', 'EVI') then
		ohs.first_event_evid
		else 9999999 end as first_event_evid,
	case when leading_system in ('KNH','ENF') then
		NULL
		when leading_system in ('WEB', 'EVI') then
		ohs.first_event_name
		else '__error' end as first_event_name,
	case when leading_system = 'KNH' then
		'Cinema'
		when leading_system = 'ENF' then
		'Deportes'
		when leading_system in ('WEB', 'EVI') then
		ohs.entry_product
		else '__error' end as entry_product,
	ci.customer_lifecycle_status as lifecycle_status,
	ci.customer_federal_region as federal_region,
	ci.customer_zip_code as zip_code,
	ci.customer_email_suffix as suffix,
	case when (ci.customer_email_suffix like '%entrada%' or ci.customer_email_suffix like '%stage%' or ci.customer_email_suffix like '%ticket%' or ci.customer_email_suffix like '%viajes%' or ci.customer_email_suffix like '%adventure%')
		then 0
	    when ((o.max_ords <= 10 and o2.max_tx <= 20) or (o.max_ords is null or o2.max_tx is null and ci.order_cnt_all = 0)) 
	    then 1
		else 0 end as honesty_status,
	ci.customer_email as pii_customer_email
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_dc_age_range ard on ard.age_range_id = ci.customer_age_range
left join zone_country_es.v_dc_gender gb1 on gb1.customer_gender_id = ci.customer_gender
left join zone_country_es.v_dc_gender gb2 on gb2.customer_gender_id = ci.estimated_gender
left join ( 
			select customer_number, max(subscription_date) as last_subscription_date
			from zone_country_es.v_dc_lnk__customer__newsletter
			where newsletter_status = 1
			group by 1	
		  ) nl on nl.customer_number = ci.customer_number
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as first_order_date,
		        max(case when rnAsc = 1 then erid end) as first_event_erid,
		        max(case when rnAsc = 1 then evid end) as first_event_evid,
		        max(case when rnAsc = 1 then event_name end) as first_event_name,
		        max(case when rnAsc = 1 then vertical end) as entry_product,
		        max(case when rnAsc = 1 then order_number end) as first_order_number
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date, erid, evid, event_name, vertical, order_number
		        from user_bsevilla.v_orders
		        ) 
		group by customer_number
		) ohs on ohs.customer_number = ci.customer_number
left join (
			select customer_number, max(ords) as max_ords
			from (
					select oh.customer_number, op.event_id, count(distinct order_number) as ords
					from zone_country_es.v_dc_order oh
					left join zone_country_es.v_dc_order_position op on oh.order_id = op.order_id
					group by 1,2
			)
			group by 1
		  ) o on o.customer_number = ci.customer_number
left join (
			select customer_number, max(tx) as max_tx
			from (
					select oh.customer_number, op.event_id, sum(oh.number_of_tickets) as tx
					from zone_country_es.v_dc_order oh
					left join zone_country_es.v_dc_order_position op on oh.order_id = op.order_id
					group by 1,2
			)
			group by 1
		  ) o2 on o2.customer_number = ci.customer_number
where ci.leading_system <> 'ENL'
--)group by 1
;

select coalesce(nullif(34,0),1)

select * from zone_country_es.v_dc_order_position limit 10;

-- old
select 
	ci.customer_number as customer_number, 
	ci.customer_id,
	ci.leading_system as leading_system,
	case when ard.age_range_name is not null then ard.age_range_name else 'undefined' end as age_range,
	case when gb1.customer_gender <> 'UNDEF' then 
		  lower(gb1.customer_gender)
		when (gb1.customer_gender = 'UNDEF' or gb1.customer_gender is null) and (gb2.customer_gender = 'UNDEF' or gb2.customer_gender is null) then 
		  'undefined'
		else lower(gb2.customer_gender)
		end as gender,
	ci.registration_date as registration_date,
	ci.reco_level as reco_level,
	ci.order_cnt_all as order_cnt,
	ci.last_order_date as last_order_date,
	ci.last_open_date_newsletter as last_open_date_newsletter,
	nl.last_subscription_date as last_subscription_date,
	case when (ci.registration_date is null and ci.last_order_date is null and ci.last_open_date_newsletter is null and nl.last_subscription_date is null) then
		ci.reference_date
		when coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') 
		  and coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		  and coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		  --and coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		  then
		ci.last_order_date
		when coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		 and coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		-- and coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		 then
		ci.last_open_date_newsletter
		when coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0') 
		 and coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		-- and coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		 then 
		nl.last_subscription_date
		when coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		 and coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0')
		 --and coalesce(ci.registration_date,'1900-01-01 00:00:00.0') > coalesce(ci.reference_date,'1900-01-01 00:00:00.0')
		 then
		ci.registration_date
		/*when coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_order_date,'1900-01-01 00:00:00.0') 
		 and coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(nl.last_subscription_date,'1900-01-01 00:00:00.0')
		 and coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(ci.last_open_date_newsletter,'1900-01-01 00:00:00.0')
		 and coalesce(ci.reference_date,'1900-01-01 00:00:00.0') > coalesce(ci.registration_date,'1900-01-01 00:00:00.0')
		then
		ci.reference_date*/
		else '1900-01-01 00:00:00.0' end as last_interaction,
	ohs.first_order_number as first_order_number,
	case when leading_system in ('KNH','ENF') then
		ci.registration_date
		when leading_system in ('WEB', 'EVI') then
		ohs.first_order_date
		else '2999-01-01 00:00:00.0' end as first_order_date,
	case when leading_system in ('KNH','ENF') then
		NULL
		when leading_system in ('WEB', 'EVI') then
		ohs.first_event_erid
		else 9999999 end as first_event_erid, 
	case when leading_system in ('KNH','ENF') then
		NULL
		when leading_system in ('WEB', 'EVI') then
		ohs.first_event_evid
		else 9999999 end as first_event_evid,
	case when leading_system in ('KNH','ENF') then
		NULL
		when leading_system in ('WEB', 'EVI') then
		ohs.first_event_name
		else '__error' end as first_event_name,
	case when leading_system = 'KNH' then
		'Cinema'
		when leading_system = 'ENF' then
		'Deportes'
		when leading_system in ('WEB', 'EVI') then
		ohs.entry_product
		else '__error' end as entry_product,
	ci.customer_lifecycle_status as lifecycle_status,
	ci.customer_federal_region as federal_region,
	ci.customer_zip_code as zip_code,
	ci.customer_email_suffix as suffix,
	case when (ci.customer_email_suffix like '%entrada%' or ci.customer_email_suffix like '%ticket%' or ci.customer_email_suffix like '%viajes%' or ci.customer_email_suffix like '%adventure%')
		then 0
	    when ((o.max_ords <= 5 and o2.max_tx <= 15) or (o.max_ords is null or o2.max_tx is null and ci.order_cnt_all = 0)) 
	    then 1
		else 0 end as honesty_status,
	ci.customer_email as pii_customer_email
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_dc_c_dim_age_range_base ard on ard.age_range_id = ci.customer_age_range
left join zone_country_es.v_dc_c_dim_gender_base gb1 on gb1.customer_gender_id = ci.customer_gender
left join zone_country_es.v_dc_c_dim_gender_base gb2 on gb2.customer_gender_id = ci.estimated_gender
left join ( 
			select customer_number, max(subscription_date) as last_subscription_date
			from zone_country_es.v_dc_lnk__customer__newsletter
			where newsletter_status = 1
			group by 1	
		  ) nl on nl.customer_number = ci.customer_number
left join 
		(select  customer_number,
		        max(case when rnAsc = 1 then order_date end) as first_order_date,
		        max(case when rnAsc = 1 then erid end) as first_event_erid,
		        max(case when rnAsc = 1 then evid end) as first_event_evid,
		        max(case when rnAsc = 1 then event_name end) as first_event_name,
		        max(case when rnAsc = 1 then vertical end) as entry_product,
		        max(case when rnAsc = 1 then order_number end) as first_order_number
		from    (
		        select  customer_number as customer_number,
		                row_number() over (partition by customer_number order by order_date) as rnAsc, 
		                order_date, erid, evid, event_name, vertical, order_number
		        from user_bsevilla.v_orders
		        ) 
		group by customer_number
		) ohs on ohs.customer_number = ci.customer_number
left join (
			select pii_customer_email, max(ords) as max_ords
			from (
					select pii_customer_email, evid, count(distinct order_number) as ords
					from user_bsevilla.v_orders oh
					group by 1,2
			)
			group by 1
		  ) o on o.pii_customer_email = ci.customer_email
left join (
			select pii_customer_email, max(tx) as max_tx
			from (
					select pii_customer_email, evid, sum(ticket_number) as tx
					from user_bsevilla.v_orders
					group by 1,2
			)
			group by 1
		  ) o2 on o2.pii_customer_email = ci.customer_email
where ci.leading_system <> 'ENL'
;



