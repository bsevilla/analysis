

select distinct ev.id evid, ev.er_id erid, ev.name1 as event_name, 
	hk.bezeichnung as vertical, 'WEB' as origin_info
from zone_country_es.v_c_tcdb__dbo__event ev
join zone_country_es.v_c_tdl__comb__vorstellung v on v.vorstellungid = ev.tdl_vorstellung_id
join zone_country_es.v_c_tdl__comb__veranstaltung ver on ver.veranstaltungid = v.veranstaltungid
join zone_country_es.v_c_tdl__comb__kategorie k on k.kategorieid = ver.kategorieid
join zone_country_es.v_c_tdl__comb__hauptkategorie hk on hk.hauptkategorieid = k.hauptkategorieid
limit 100;

select distinct event_id evid, eventseries_id erid, event_name1 as event_name, 
	case when event_name3 = 'Teatro' then 'Culture'
	when event_name3 = 'Musicales And Shows' then 'Musical'
	when event_name3 in ('Parques', 'Exposiciones') then 'Otros'
	when event_name3 = 'Deportes' then 'Deportes'
	when event_name3 = 'CINE' then 'Cinema'
	when event_name3 = 'Conciertos' then 'M�sica'
	else 'error' end as vertical, 'ENL' as origin_info
from zone_country_es.t_legacy_test__dim_event
limit 100;

select distinct 
	event_id as evid,
	eventseries_id as erid,
	event_name1 as event_name, 
	'Deportes' as vertical,
	'ENF' as origin_info
from zone_country_es.v_c_entradas__football__dim_event
limit 100;

select ev.*--event_id, ev.event_series_id,enlev.event_id, enlev.eventseries_id
--select count(case when enlev.event_id is null then ev.event_id end) city_no_federal
from zone_country_es.v_c_dama_event ev
left join zone_country_es.t_legacy_test__dim_event enlev on ev.event_name1 = enlev.event_name1
where 1=1
--and ev.data_source = 'ENL'
and enlev.event_id is null
--order by rand()
--limit 1000
;

select * from zone_country_es.t_legacy_test__dim_event
limit 100;

