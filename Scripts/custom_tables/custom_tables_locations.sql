--drop table user_bsevilla.v_dim_cinema_locations;
create or replace table user_bsevilla.v_dim_cinema_locations as (
														select v.*, cp.latitud as latitude, cp.longitud as longitude, cp.geo as geo
														from zone_country_es.v_c_kinoheld__dim_venue v
														left join user_bsevilla.v_dim_cp cp on v.venue_postal_code = cp.cp
													);


select * from user_bsevilla.v_dim_cinema_locations limit 10;



select a.venue_name, b.venue_name, st_distance(ST_Transform(a.geo, 2163),
     ST_Transform(b.geo, 2163)) FROM user_bsevilla.v_dim_cinema_locations a, user_bsevilla.v_dim_cinema_locations b
order by 1,2
     limit 10;

