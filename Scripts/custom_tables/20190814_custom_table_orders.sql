create or replace view user_bsevilla.v_orders as
select distinct
	ci.customer_number as customer_number, 
	convert(varchar(40), oh.ordernummer) as order_number,
	oh.orderdatum as order_date,
	oh.number_of_tickets as ticket_number,
	oh.order_total as order_total,
	ku.id as artist_id,
	ku.name as artist_name,
	op.er_id as erid,
	op.ev_id as evid,
	ev.name1 as event_name, 
	hk.bezeichnung as vertical,
	ci.customer_email as pii_customer_email,
	'WEB' as origin_info
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_c_tcdb__dbo__order_header oh on oh.kundennummer = ci.customer_number 
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.id = op.oh_id
left join zone_country_es.v_c_tcdb__dbo__event ev on ev.id = op.ev_id
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link ek on op.er_id = ek.er_id
left join zone_country_es.v_c_tcdb__dbo__kuenstler ku on ku.id = ek.ku_id
join zone_country_es.v_c_tdl__comb__vorstellung v on v.vorstellungid = ev.tdl_vorstellung_id
join zone_country_es.v_c_tdl__comb__veranstaltung ver on ver.veranstaltungid = v.veranstaltungid
join zone_country_es.v_c_tdl__comb__kategorie k on k.kategorieid = ver.kategorieid
join zone_country_es.v_c_tdl__comb__hauptkategorie hk on hk.hauptkategorieid = k.hauptkategorieid
union
select distinct
	ci.customer_number as customer_number, 
	convert(varchar(40), oh.order_number) as order_number,
	to_timestamp(oh.order_date) as order_date,
	op.seat_cnt as ticket_number,
	ot.order_total as order_total,
	ea.artist_id as artist_id,
	da.artist_name as artist_name,
	de.eventseries_id as erid,
	op.event_id as evid,
	de.event_name1 as event_name, 
	'Deportes' as vertical,
	ci.customer_email as pii_customer_email,
	'ENF' as origin_info
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_c_entradas__football__fact_order oh on oh.pii_customer_email = ci.customer_email
left join zone_country_es.v_c_entradas__football__fact_order_position op on op.order_id = oh.order_id
left join (
			select op.order_position_id, sum(ot.ticket_price_sold) as order_total
			from zone_country_es.v_c_entradas__football__fact_order_position op
			left join zone_country_es.v_c_entradas__football__fact_order_ticket ot on ot.order_position_id = op.order_position_id
			group by 1
			) ot on ot.order_position_id = op.order_position_id
left join zone_country_es.v_c_entradas__football__link_event_artist ea on op.event_id = ea.event_id
left join zone_country_es.v_c_entradas__football__dim_event de on op.event_id = de.event_id
left join zone_country_es.v_c_entradas__football__dim_artist da on ea.artist_id = da.artist_id
where order_number is not null
;

-- only web:
select distinct
	ci.customer_number as customer_number, 
	convert(varchar(40), oh.ordernummer) as order_number,
	oh.orderdatum as order_date,
	oh.number_of_tickets as ticket_number,
	oh.order_total as order_total,
	ku.id as artist_id,
	ku.name as artist_name,
	op.er_id as erid,
	op.ev_id as evid,
	ev.name1 as event_name, 
	hk.bezeichnung as vertical,
	ci.customer_email as pii_customer_email,
	'WEB' as origin_info
from zone_country_es.v_c_dama_ci ci
left join zone_country_es.v_c_tcdb__dbo__order_header oh on oh.kundennummer = ci.customer_number 
left join zone_country_es.v_c_tcdb__dbo__order_position op on oh.id = op.oh_id
left join zone_country_es.v_c_tcdb__dbo__event ev on ev.id = op.ev_id
left join zone_country_es.v_c_tcdb__dbo__eventreihe_kuenstler_link ek on op.er_id = ek.er_id
left join zone_country_es.v_c_tcdb__dbo__kuenstler ku on ku.id = ek.ku_id
join zone_country_es.v_c_tdl__comb__vorstellung v on v.vorstellungid = ev.tdl_vorstellung_id
join zone_country_es.v_c_tdl__comb__veranstaltung ver on ver.veranstaltungid = v.veranstaltungid
join zone_country_es.v_c_tdl__comb__kategorie k on k.kategorieid = ver.kategorieid
join zone_country_es.v_c_tdl__comb__hauptkategorie hk on hk.hauptkategorieid = k.hauptkategorieid
;

select distinct
	o.customer_number as customer_number, 
	o.order_number as order_number,
	o.order_date as order_date,
	case when basket_size is null then round(zeroifnull(order_value/nullifzero(ticket_price+fee_sum))) else o.basket_size end as ticket_number,
	oh.order_total as order_total,
	o.artist_id as artist_id,
	da.artist_name as artist_name,
	o.event_series_id as erid,
	o.event_id as evid,
	ev.event_name1 as event_name, 
	hk.bezeichnung as vertical,
	ci.customer_email as pii_customer_email,
	'WEB' as origin_info
from zone_country_es.v_fact_order_base o
left join zone_country_es.v_c_dama_artist da on o.artist_id = da.artist_id
left join zone_country_es.v_c_dama_event ev on ev.event_id = o.event_id
join zone_country_es.v_c_tdl__comb__vorstellung v on v.vorstellungid = ev.tdl_vorstellung_id
join zone_country_es.v_c_tdl__comb__veranstaltung ver on ver.veranstaltungid = v.veranstaltungid
join zone_country_es.v_c_tdl__comb__kategorie k on k.kategorieid = ver.kategorieid
join zone_country_es.v_c_tdl__comb__hauptkategorie hk on hk.hauptkategorieid = k.hauptkategorieid
limit 10;

--round(zeroifnull(order_value/nullifzero(ticket_price+fee_sum))), basket_size,  
select o.event_id, da.*
from zone_country_es.v_fact_order_base o
left join zone_country_es.v_c_dama_event da on o.event_id = da.event_id
where o.source_name = 'KNH'
and o.customer_number > 0
and o.event_id is not null
limit 10
;

select 1 = 2

select number_of_tickets from zone_country_es.v_c_tcdb__dbo__order_header where ordernummer in (1606505223,1608019071,1607780935,1607005411);